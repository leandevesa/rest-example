# TP TDD - Leandro Devesa (95637)

File uploader supporting invitations through users signup/auth

## Run with docker

docker-compose up --build

## Run locally with gradle

#### Requirements

- mysql 8+
- set environment variables:
   -    MYSQL_ROOT_PASSWORD
   -    MYSQL_HOST
   -    MAIL_USERNAME
   -    MAIL_PASSWORD

#### Building and running

gradle build

java -jar build/libs/rest-service-0.0.1-SNAPSHOT.jar

## OAuth login

http://localhost:8080/login

## API Example

See attached postman collections

# Documentación arquitectura 4+1

Se encuentra ajuntado en el repositorio el archivo "Arquitectura 4+1" en el cual se detalla la arquitectura de la aplicación