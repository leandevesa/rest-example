package com.example.restservice.filter;

import com.example.restservice.service.JWTService;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JWTAuthorizationFilterTest {

    @InjectMocks
    private JWTAuthorizationFilter jwtAuthorizationFilter;
    @Mock
    private JWTService jwtService;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private HttpServletResponse httpServletResponse;
    @Mock
    private FilterChain filterChain;

    private final static String HEADER = "Authorization";
    private final static String PREFIX = "Bearer ";
    private final static String TOKEN = PREFIX + "123";

    @Test
    void doFilterInternal_validatesToken_ifHasTokenHeader() throws IOException {

        when(httpServletRequest.getHeader(HEADER)).thenReturn(TOKEN);

        jwtAuthorizationFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        verify(jwtService).validateToken(TOKEN.replace(PREFIX, ""));
    }

    @Test
    void doFilterInternal_doesNothing_ifDoesntHaveTokenHeader() throws IOException {

        jwtAuthorizationFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        verifyNoInteractions(jwtService);
    }
}