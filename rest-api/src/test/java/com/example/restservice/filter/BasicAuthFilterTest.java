package com.example.restservice.filter;

import com.example.restservice.exception.InvalidCredentialsException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserNotVerifiedException;
import com.example.restservice.service.AuthenticatedUserService;
import com.example.restservice.service.BasicAuthService;
import com.example.restservice.service.JWTService;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BasicAuthFilterTest {

    @InjectMocks
    private BasicAuthFilter basicAuthFilter;
    @Mock
    private BasicAuthService basicAuthService;
    @Mock
    private AuthenticatedUserService authenticatedUserService;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private HttpServletResponse httpServletResponse;
    @Mock
    private FilterChain filterChain;

    private final static String HEADER = "Authorization";
    private final static String PREFIX = "Basic ";
    private final static String TOKEN = PREFIX + "123";

    @Test
    void doFilterInternal_validatesToken_ifHasTokenHeader() throws IOException, NotFoundException, InvalidCredentialsException,
                                                                   UserNotVerifiedException {

        when(authenticatedUserService.isAuthenticated()).thenReturn(false);
        when(httpServletRequest.getHeader(HEADER)).thenReturn(TOKEN);

        basicAuthFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        verify(basicAuthService).getValidUsername(TOKEN.replace(PREFIX, ""));
    }

    @Test
    void doFilterInternal_doesNothing_ifDoesntHaveTokenHeader() throws IOException {

        when(authenticatedUserService.isAuthenticated()).thenReturn(false);
        basicAuthFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        verifyNoInteractions(basicAuthService);
    }

    @Test
    void doFilterInternal_doesNothing_ifIsAlreadyAuthenticated() throws IOException {

        when(authenticatedUserService.isAuthenticated()).thenReturn(true);
        basicAuthFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        verifyNoInteractions(basicAuthService);
    }
}