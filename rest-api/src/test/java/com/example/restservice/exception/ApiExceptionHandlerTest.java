package com.example.restservice.exception;

import com.example.restservice.dto.GenericApiResponseDto;
import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ApiExceptionHandlerTest {

    @InjectMocks
    private ApiExceptionHandler apiExceptionHandler;
    @Mock
    private HttpServletRequest httpServletRequest;

    private final static ApiException API_EXCEPTION = new InvalidTokenException();

    @Test
    void apiExceptionHandler() {

        ResponseEntity<GenericApiResponseDto> genericApiResponseDto =
            apiExceptionHandler.apiExceptionHandler(httpServletRequest, API_EXCEPTION);

        assertEquals(API_EXCEPTION.getStatusCode(), genericApiResponseDto.getStatusCode().value());
        assertEquals(API_EXCEPTION.getMessage(), genericApiResponseDto.getBody().getMessage());
        assertEquals(API_EXCEPTION.getStatusCode(), genericApiResponseDto.getBody().getStatus());
    }
}