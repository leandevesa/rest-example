package com.example.restservice.service;

import com.example.restservice.model.Invitation;
import com.example.restservice.model.Permission;
import com.example.restservice.model.PermissionMode;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import com.example.restservice.model.VerificationToken;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
class MailServiceTest {

    @InjectMocks
    private MailService mailService;
    @Mock
    private MailSenderService mailSenderService;

    @Mock
    private Invitation invitation;
    @Mock
    private User fromUser;
    @Mock
    private User toUser;
    @Mock
    private Resource resource;
    @Mock
    private VerificationToken verificationToken;

    private final static String EMAIL = "mail@mail.com";
    private final static PermissionMode PERMISSION_MODE = PermissionMode.READ_WRITE;

    @Test
    void sendInvitation() {
        when(invitation.getFromUser()).thenReturn(fromUser);
        when(invitation.getToUser()).thenReturn(toUser);
        when(invitation.getResource()).thenReturn(resource);
        when(invitation.getPermissionMode()).thenReturn(PERMISSION_MODE);
        when(toUser.getEmail()).thenReturn(EMAIL);

        mailService.sendInvitation(invitation);

        ArgumentCaptor<String> receiverEmail = ArgumentCaptor.forClass(String.class);

        verify(mailSenderService).send(receiverEmail.capture(), any(), any());

        assertEquals(EMAIL, receiverEmail.getValue());
    }

    @Test
    void sendVerificationToken() {
        when(verificationToken.getUser()).thenReturn(toUser);
        when(toUser.getEmail()).thenReturn(EMAIL);

        mailService.sendVerificationToken(verificationToken);

        ArgumentCaptor<String> receiverEmail = ArgumentCaptor.forClass(String.class);

        verify(mailSenderService).send(receiverEmail.capture(), any(), any());

        assertEquals(EMAIL, receiverEmail.getValue());
    }
}