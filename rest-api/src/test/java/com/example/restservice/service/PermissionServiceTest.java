package com.example.restservice.service;

import com.example.restservice.exception.NotFoundException;
import com.example.restservice.model.Permission;
import com.example.restservice.model.PermissionMode;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import com.example.restservice.model.VerificationToken;
import com.example.restservice.repository.PermissionRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.mockito.ArgumentCaptor;

@ExtendWith(MockitoExtension.class)
class PermissionServiceTest {

    @InjectMocks
    private PermissionService permissionService;
    @Mock
    private PermissionRepository permissionRepository;

    @Mock
    private User user;
    @Mock
    private Resource resource;
    @Mock
    private Permission permission;

    private final static PermissionMode PERMISSION_MODE = PermissionMode.READ;

    @Test
    void add() {
        ArgumentCaptor<Permission> permissionArgumentCaptor = ArgumentCaptor.forClass(Permission.class);

        permissionService.add(user, PERMISSION_MODE, resource);

        verify(permissionRepository).save(permissionArgumentCaptor.capture());

        Permission persistedPermission = permissionArgumentCaptor.getValue();

        assertEquals(PERMISSION_MODE, persistedPermission.getPermissionMode());
        assertEquals(resource, persistedPermission.getResource());
        assertEquals(user, persistedPermission.getUser());
    }

    @Test
    void remove() throws NotFoundException {
        when(permissionRepository.findByUserAndResource(user, resource)).thenReturn(Optional.of(permission));

        permissionService.remove(user, resource);

        verify(permissionRepository).delete(permission);
    }

    @Test
    void remove_shouldThrowExceptionWhenPermissionNotFound() {
        when(permissionRepository.findByUserAndResource(user, resource)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> permissionService.remove(user, resource));
    }

    @Test
    void hasAccessToResource_shouldReturnTrue_whenRegisterExists() {
        when(permissionRepository.findByUserAndResource(user, resource)).thenReturn(Optional.of(permission));

        assertTrue(permissionService.hasAccessToResource(user, resource));
    }

    @Test
    void hasAccessToResource_shouldReturnFalse_whenRegisterDoesntExist() {
        when(permissionRepository.findByUserAndResource(user, resource)).thenReturn(Optional.empty());

        assertFalse(permissionService.hasAccessToResource(user, resource));
    }

    @Test
    void hasWriteAccessToResource_shouldReturnTrue_whenRegisterExists_andItIsReadWrite() {
        when(permissionRepository.findByUserAndResource(user, resource)).thenReturn(Optional.of(permission));
        when(permission.getPermissionMode()).thenReturn(PermissionMode.READ_WRITE);

        assertTrue(permissionService.hasWriteAccessToResource(user, resource));
    }

    @Test
    void hasWriteAccessToResource_shouldReturnFalse_whenRegisterExists_andItIsRead() {
        when(permissionRepository.findByUserAndResource(user, resource)).thenReturn(Optional.of(permission));
        when(permission.getPermissionMode()).thenReturn(PermissionMode.READ);

        assertFalse(permissionService.hasWriteAccessToResource(user, resource));
    }

    @Test
    void hasWriteAccessToResource_shouldReturnFalse_whenRegisterDoesntExist() {
        when(permissionRepository.findByUserAndResource(user, resource)).thenReturn(Optional.empty());

        assertFalse(permissionService.hasWriteAccessToResource(user, resource));
    }
}