package com.example.restservice.service;

import com.example.restservice.exception.InvalidCredentialsException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserNotVerifiedException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import java.util.Base64;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BasicAuthServiceTest {

    @InjectMocks
    private BasicAuthService basicAuthService;
    @Mock
    private UserService userService;

    private final static String USERNAME = "unuser";
    private final static String HEADER_VALUE = USERNAME + ":unapass";

    @Test
    void getValidUsername() throws NotFoundException, InvalidCredentialsException, UserNotVerifiedException {
        String username = basicAuthService.getValidUsername(
            Base64.getEncoder().encodeToString(HEADER_VALUE.getBytes()));

        assertEquals(USERNAME, username);
    }
}