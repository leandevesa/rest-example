package com.example.restservice.service;

import com.example.restservice.dto.resource.ResourceDto;
import com.example.restservice.dto.resource.ResourcePublicDto;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import com.example.restservice.repository.ResourceRepository;
import com.example.restservice.translator.ResourceTranslator;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.ArgumentCaptor;

@ExtendWith(MockitoExtension.class)
class ResourceServiceTest {

    @InjectMocks
    private ResourceService resourceService;
    @Mock
    private ResourceRepository resourceRepository;
    @Mock
    private FileService fileService;
    @Mock
    private UserCRUDService userCRUDService;
    @Mock
    private AuthenticatedUserService authenticatedUserService;
    @Mock
    private PermissionService permissionService;
    @Mock
    private ResourceTranslator resourceTranslator;
    @Mock
    private Resource persistedResource;
    @Mock
    private User user;
    @Mock
    private User anotherUser;
    @Mock
    private ResourceDto resourceDto;

    private final static String USERNAME = "username";
    private final static String RESOURCE_NAME = "resourceName";
    private final static long RESOURCE_ID = 123L;
    private final static String DESCRIPTION = "description";
    private final static String CONTENT_TYPE = "content/type";
    private final static String FILE_NAME = "fileName";
    private final static String RESOURCE_PATH = "resourcePath";
    private final static boolean IS_PUBLIC = true;
    private final static byte[] FILE = { (byte)0xe0 };

    @Test
    void save() throws IOException, NotFoundException {

        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);

        when(resourceRepository.save(any())).thenReturn(persistedResource);
        when(persistedResource.getId()).thenReturn(RESOURCE_ID);
        when(resourceTranslator.toResourceDto(persistedResource)).thenReturn(resourceDto);

        ResourceDto savedResource = resourceService.save(USERNAME, RESOURCE_NAME, DESCRIPTION, IS_PUBLIC, CONTENT_TYPE, FILE_NAME, FILE);

        ArgumentCaptor<Resource> resourceArgumentCaptor = ArgumentCaptor.forClass(Resource.class);

        verify(resourceRepository).save(resourceArgumentCaptor.capture());

        Resource resource = resourceArgumentCaptor.getValue();

        assertEquals(user, resource.getUser());
        assertEquals(RESOURCE_NAME, resource.getName());
        assertEquals(DESCRIPTION, resource.getDescription());
        assertEquals(IS_PUBLIC, resource.getIsPublic());
        assertEquals(CONTENT_TYPE, resource.getContentType());
        assertEquals(FILE_NAME, resource.getFileName());

        verify(fileService).saveToDisk(FILE, RESOURCE_ID);

        assertEquals(resourceDto, savedResource);
    }

    @Test
    void update_throwsException_ifResourceIsNotFound() {

        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> resourceService.update(USERNAME, RESOURCE_ID, CONTENT_TYPE, FILE_NAME, FILE));
    }

    @Test
    void update_throwsException_ifUserDoesntHaveWriteAccess() throws NotFoundException {

        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));

        assertThrows(UserWithoutPermissionsException.class, () -> resourceService.update(USERNAME, RESOURCE_ID, CONTENT_TYPE, FILE_NAME, FILE));
    }

    @Test
    void update() throws IOException, NotFoundException, UserWithoutPermissionsException {

        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));
        when(persistedResource.getId()).thenReturn(RESOURCE_ID);

        when(permissionService.hasWriteAccessToResource(user, persistedResource)).thenReturn(true);

        resourceService.update(USERNAME, RESOURCE_ID, CONTENT_TYPE, FILE_NAME, FILE);

        ArgumentCaptor<Resource> resourceArgumentCaptor = ArgumentCaptor.forClass(Resource.class);

        verify(resourceRepository).save(resourceArgumentCaptor.capture());

        Resource updatedResource = resourceArgumentCaptor.getValue();

        assertEquals(CONTENT_TYPE, updatedResource.getContentType());
        assertEquals(FILE_NAME, updatedResource.getFileName());

        verify(fileService).saveToDisk(FILE, RESOURCE_ID);
    }

    @Test
    void delete_throwsException_ifItIsNotResourceCreator() throws IOException, NotFoundException, UserWithoutPermissionsException {
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));
        when(persistedResource.getUser()).thenReturn(user);
        when(user.getUsername()).thenReturn("otherUser");

        assertThrows(UserWithoutPermissionsException.class, () -> resourceService.delete(USERNAME, RESOURCE_ID));
    }

    @Test
    void delete() throws IOException, NotFoundException, UserWithoutPermissionsException {
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));
        when(persistedResource.getUser()).thenReturn(user);
        when(user.getUsername()).thenReturn(USERNAME);
        when(persistedResource.getId()).thenReturn(RESOURCE_ID);

        resourceService.delete(USERNAME, RESOURCE_ID);

        verify(resourceRepository).delete(persistedResource);
        verify(fileService).deleteFromDisk(RESOURCE_ID);
    }

    @Test
    void getAllPublicResources() {
        when(persistedResource.getId()).thenReturn(RESOURCE_ID);
        when(persistedResource.getName()).thenReturn(RESOURCE_NAME);
        when(persistedResource.getDescription()).thenReturn(DESCRIPTION);

        when(resourceRepository.findByIsPublic(true)).thenReturn(Lists.newArrayList(persistedResource));

        List<ResourcePublicDto> resourcePublicDtoList = resourceService.getAllPublicResources();

        assertEquals(1, resourcePublicDtoList.size());

        ResourcePublicDto resourcePublicDto = resourcePublicDtoList.get(0);

        assertEquals(RESOURCE_ID, resourcePublicDto.getId());
        assertEquals(DESCRIPTION, resourcePublicDto.getDescription());
        assertEquals(RESOURCE_NAME, resourcePublicDto.getName());
    }

    @Test
    void getResource() throws NotFoundException {
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));

        assertEquals(persistedResource, resourceService.getResource(RESOURCE_ID));
    }

    @Test
    void getGrantedResource_throwsException_ifUserDoesntHaveAccess() throws NotFoundException {
        when(persistedResource.getUser()).thenReturn(anotherUser);
        when(anotherUser.getUsername()).thenReturn("anotherUserName");
        when(authenticatedUserService.getUsername()).thenReturn(USERNAME);
        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);
        when(user.getUsername()).thenReturn(USERNAME);
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));
        when(permissionService.hasAccessToResource(user, persistedResource)).thenReturn(false);

        assertThrows(UserWithoutPermissionsException.class, () -> resourceService.getGrantedResource(RESOURCE_ID));
    }

    @Test
    void getGrantedResource() throws NotFoundException, UserWithoutPermissionsException {
        when(persistedResource.getUser()).thenReturn(anotherUser);
        when(anotherUser.getUsername()).thenReturn("anotherUserName");
        when(authenticatedUserService.getUsername()).thenReturn(USERNAME);
        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);
        when(user.getUsername()).thenReturn(USERNAME);
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));
        when(permissionService.hasAccessToResource(user, persistedResource)).thenReturn(true);

        assertEquals(persistedResource, resourceService.getGrantedResource(RESOURCE_ID));
    }

    @Test
    void removePermission_throwsException_ifItIsNotResourceCreator() throws NotFoundException {
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));
        when(persistedResource.getUser()).thenReturn(anotherUser);
        when(anotherUser.getUsername()).thenReturn("anotherUserName");
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));

        assertThrows(UserWithoutPermissionsException.class, () -> resourceService.removePermission(USERNAME, RESOURCE_ID, "anotherUserName"));
    }

    @Test
    void removePermission() throws NotFoundException, UserWithoutPermissionsException {
        when(resourceRepository.findById(RESOURCE_ID)).thenReturn(Optional.of(persistedResource));
        when(persistedResource.getUser()).thenReturn(user);
        when(user.getUsername()).thenReturn(USERNAME);

        when(userCRUDService.findByUsername("anotherUserName")).thenReturn(anotherUser);

        resourceService.removePermission(USERNAME, RESOURCE_ID, "anotherUserName");

        verify(permissionService).remove(anotherUser, persistedResource);
    }

    @Test
    void getResourcePath() {
        when(fileService.getResourcePath(RESOURCE_ID)).thenReturn(RESOURCE_PATH);

        assertEquals(RESOURCE_PATH, resourceService.getResourcePath(RESOURCE_ID));
    }
}