package com.example.restservice.service;

import com.example.restservice.model.User;
import com.example.restservice.model.UserProfile;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserMergerServiceTest {

    @InjectMocks
    private UserMergerService userMergerService;
    @Mock
    private User oldUser;
    @Mock
    private User newUser;
    @Mock
    private UserProfile oldUserProfile;
    @Mock
    private UserProfile newUserProfile;

    private final static String OLD_NAME = "oldName";
    private final static String NEW_NAME = "newName";
    private final static String OLD_EMAIL = "oldEmail";
    private final static String NEW_EMAIL = "newEmail";
    private final static String OLD_AVATAR = "oldAvatar";

    @Test
    void merge_onlyUpdates_nonNullAttributes() {
        when(oldUser.getUserProfile()).thenReturn(oldUserProfile);
        when(newUser.getUserProfile()).thenReturn(newUserProfile);

        when(oldUser.getEmail()).thenReturn(OLD_EMAIL);
        when(newUser.getEmail()).thenReturn(NEW_EMAIL);

        when(oldUserProfile.getAvatar()).thenReturn(OLD_AVATAR);
        when(oldUserProfile.getName()).thenReturn(OLD_NAME);
        when(newUserProfile.getName()).thenReturn(NEW_NAME);

        User mergedUser = userMergerService.merge(oldUser, newUser);

        assertEquals(NEW_NAME, mergedUser.getUserProfile().getName());
        assertEquals(OLD_AVATAR, mergedUser.getUserProfile().getAvatar());
        assertEquals(NEW_EMAIL, mergedUser.getEmail());
    }
}