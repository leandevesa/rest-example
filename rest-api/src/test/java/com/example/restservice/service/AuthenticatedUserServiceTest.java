package com.example.restservice.service;

import java.util.Collection;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthenticatedUserServiceTest {

    @InjectMocks
    private AuthenticatedUserService authenticatedUserService;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Authentication authentication;

    private final static String USERNAME = "username";

    @Test
    void isValidUser_shouldReturnTrueWhenUsernameIsSameAsLoggedIn() {
        assertTrue(authenticatedUserService.isValidUser(USERNAME));
    }

    @Test
    void isValidUser_shouldReturnFalseWhenUsernameIsNotTheOneLoggedIn() {
        assertFalse(authenticatedUserService.isValidUser("another user"));
    }

    @Test
    void getUsername() {
        assertEquals(USERNAME, authenticatedUserService.getUsername());
    }

    @BeforeEach
    void setLoggedInUser() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(USERNAME);

        SecurityContextHolder.setContext(securityContext);
    }
}