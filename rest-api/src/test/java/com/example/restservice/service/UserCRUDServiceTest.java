package com.example.restservice.service;

import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UsernameTakenException;
import com.example.restservice.model.User;
import com.example.restservice.repository.UserRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class UserCRUDServiceTest {

    @InjectMocks
    private UserCRUDService userCRUDService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private User user;
    @Mock
    private User persistedUser;

    private final static String USERNAME = "username";

    @Test
    void save_shouldReturnPersistedUser() {

        when(userRepository.save(user)).thenReturn(persistedUser);

        User crudUser = userCRUDService.save(user);

        assertEquals(persistedUser, crudUser);
    }

    @Test
    void save_shouldThrowUsernameTakenException_whenUsernameAlreadyExists() {

        when(userRepository.save(user)).thenThrow(new DataIntegrityViolationException(""));

        assertThrows(UsernameTakenException.class, () -> userCRUDService.save(user));
    }

    @Test
    void update_shouldCallRepositorySave() {

        userRepository.save(user);

        verify(userRepository).save(user);
    }

    @Test
    void findByUsername_shouldReturnPersistedUser() throws NotFoundException {

        when(userRepository.findByUsernameAndDeletedFalse(USERNAME)).thenReturn(Optional.of(persistedUser));

        User crudUser = userCRUDService.findByUsername(USERNAME);

        assertEquals(persistedUser, crudUser);
    }

    @Test
    void findByUsername_shouldThrowNotFoundException_whenUserDoesntExist() throws NotFoundException {

        when(userRepository.findByUsernameAndDeletedFalse(USERNAME)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> userCRUDService.findByUsername(USERNAME));
    }

    @Test
    void findAll_shouldCallRepository_findAllNonDeleted() {

        userCRUDService.findAll();

        verify(userRepository).findByDeletedFalse();
    }
}