package com.example.restservice.service;

import com.example.restservice.dto.user.UserDto;
import com.example.restservice.dto.user.UserLoginResponseDto;
import com.example.restservice.dto.user.UserPublicProfileDto;
import com.example.restservice.dto.user.UserRegisterRequestDto;
import com.example.restservice.exception.InvalidCredentialsException;
import com.example.restservice.exception.InvalidTokenException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserNotVerifiedException;
import com.example.restservice.exception.UsernameTakenException;
import com.example.restservice.model.Permission;
import com.example.restservice.model.User;
import com.example.restservice.model.UserProfile;
import com.example.restservice.model.VerificationToken;
import com.example.restservice.translator.UserTranslator;
import java.util.List;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.ArgumentCaptor;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;
    @Mock
    private UserCRUDService userCRUDService;
    @Mock
    private UserTranslator userTranslator;
    @Mock
    private UserMergerService userMergerService;
    @Mock
    private VerificationTokenService verificationTokenService;
    @Mock
    private JWTService jwtService;
    @Mock
    private MailService mailService;
    @Mock
    private PasswordHashingService passwordHashingService;

    @Mock
    private UserRegisterRequestDto userRegisterRequestDto;
    @Mock
    private User user;
    @Mock
    private User anotherUser;
    @Mock
    private User modifiedUser;
    @Mock
    private VerificationToken verificationToken;
    @Mock
    private UserDto userDto;
    @Mock
    private UserPublicProfileDto userPublicProfileDto;
    @Mock
    private UserProfile userProfile;

    private final static String USERNAME = "username";
    private final static String NAME = "name";
    private final static String LASTNAME = "lastname";
    private final static String AVATAR = "avatar";
    private final static String TOKEN = "token";
    private final static String PASSWORD = "password";
    private final static String HASHED_PASSWORD = "hashedPassword";

    @Test
    void register() throws UsernameTakenException {

        when(userTranslator.fromUserRegisterDto(userRegisterRequestDto)).thenReturn(user);
        when(userCRUDService.save(user)).thenReturn(user);
        when(verificationTokenService.create(user)).thenReturn(verificationToken);

        userService.register(userRegisterRequestDto);

        verify(mailService).sendVerificationToken(verificationToken);
    }

    @Test
    void verifyUser_throwsException_ifTokenIsInvalid() throws NotFoundException, InvalidTokenException {
        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);

        assertThrows(InvalidTokenException.class, () -> userService.verify(USERNAME, TOKEN));
    }

    @Test
    void verifyUser() throws NotFoundException, InvalidTokenException {
        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);
        when(verificationTokenService.isTokenValid(user, TOKEN)).thenReturn(true);

        userService.verify(USERNAME, TOKEN);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);

        verify(userCRUDService).update(userArgumentCaptor.capture());

        assertTrue(userArgumentCaptor.getValue().isVerified());
    }

    @Test
    void get() throws NotFoundException {
        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);
        when(userTranslator.toUserDto(user)).thenReturn(userDto);

        assertEquals(userDto, userService.get(USERNAME));
    }

    @Test
    void getPublicUserList() {
        when(userCRUDService.findAll()).thenReturn(Lists.newArrayList(user, anotherUser));
        when(user.getUsername()).thenReturn(USERNAME);
        when(anotherUser.getUsername()).thenReturn("anotherUser");
        when(anotherUser.getUserProfile()).thenReturn(userProfile);
        when(userProfile.getName()).thenReturn(NAME);
        when(userProfile.getAvatar()).thenReturn(AVATAR);
        when(userProfile.getLastname()).thenReturn(LASTNAME);

        List<UserPublicProfileDto> userPublicProfileDtoList = userService.getPublicUserList(USERNAME);

        assertEquals(1, userPublicProfileDtoList.size());

        UserPublicProfileDto userPublicProfileDto = userPublicProfileDtoList.get(0);

        assertEquals(NAME, userPublicProfileDto.getName());
        assertEquals(LASTNAME, userPublicProfileDto.getLastname());
        assertEquals(AVATAR, userPublicProfileDto.getAvatar());
    }

    @Test
    void delete() throws NotFoundException {

        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);

        userService.delete(USERNAME);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);

        verify(userCRUDService).update(userArgumentCaptor.capture());

        assertTrue(userArgumentCaptor.getValue().isDeleted());
    }

    @Test
    void login_throwsException_ifPasswordsDoesntMatch() throws NotFoundException {

        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);

        when(user.getPassword()).thenReturn(PASSWORD);

        assertThrows(InvalidCredentialsException.class, () -> userService.login(USERNAME, "wrongPassword"));
    }

    @Test
    void login_throwsException_ifUserIsNotVerified() throws NotFoundException {

        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);

        when(user.getPassword()).thenReturn(HASHED_PASSWORD);
        when(user.isVerified()).thenReturn(false);
        when(passwordHashingService.passwordsMatch(PASSWORD, HASHED_PASSWORD)).thenReturn(true);

        assertThrows(UserNotVerifiedException.class, () -> userService.login(USERNAME, PASSWORD));
    }

    @Test
    void login() throws NotFoundException, InvalidCredentialsException, UserNotVerifiedException {

        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);

        when(user.getPassword()).thenReturn(HASHED_PASSWORD);
        when(user.isVerified()).thenReturn(false);
        when(jwtService.getJWTToken(USERNAME)).thenReturn(TOKEN);
        when(user.isVerified()).thenReturn(true);
        when(passwordHashingService.passwordsMatch(PASSWORD, HASHED_PASSWORD)).thenReturn(true);

        UserLoginResponseDto userLoginResponseDto = userService.login(USERNAME, PASSWORD);

        assertEquals(TOKEN, userLoginResponseDto.getToken());
        assertEquals(USERNAME, userLoginResponseDto.getUsername());
    }

    @Test
    void update() throws NotFoundException {

        when(userCRUDService.findByUsername(USERNAME)).thenReturn(user);
        when(userTranslator.fromUserDto(userDto, false)).thenReturn(anotherUser);
        when(userMergerService.merge(user, anotherUser)).thenReturn(modifiedUser);
        when(userTranslator.toUserDto(modifiedUser)).thenReturn(userDto);

        UserDto updatedUser = userService.update(USERNAME, userDto);

        verify(userCRUDService).update(modifiedUser);

        assertEquals(userDto, updatedUser);
    }
}