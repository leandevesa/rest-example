package com.example.restservice.service;

import io.jsonwebtoken.Claims;
import java.time.Duration;
import java.util.Date;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class JWTServiceTest {

    @InjectMocks
    private JWTService jwtService;

    private final static String USERNAME = "username";
    private final static int TOKEN_DURATION_HOURS = 1;

    @Test
    void get_andValidate_token() {
        String token = jwtService.getJWTToken(USERNAME);

        Claims claims = jwtService.validateToken(token.replace("Bearer ", ""));

        Date expirationDate = claims.getExpiration();

        long tokenDurationInMs =
            Duration.between(new Date().toInstant(), expirationDate.toInstant()).toMillis();

        int tokenDurationInHoursRoundedUp =
            Math.round((float) tokenDurationInMs / 3600000);

        assertEquals(TOKEN_DURATION_HOURS, tokenDurationInHoursRoundedUp);
        assertEquals(USERNAME, claims.getSubject());
        assertEquals(0, token.indexOf("Bearer"));
    }
}