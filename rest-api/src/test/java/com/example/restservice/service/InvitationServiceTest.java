package com.example.restservice.service;

import com.example.restservice.dto.invitation.InviteUserDto;
import com.example.restservice.dto.invitation.PendingInvitationDto;
import com.example.restservice.dto.invitation.RequestAccessDto;
import com.example.restservice.exception.InvalidTokenException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import com.example.restservice.model.Invitation;
import com.example.restservice.model.InvitationType;
import com.example.restservice.model.PermissionMode;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import com.example.restservice.repository.InvitationRepository;
import com.example.restservice.translator.InvitationTranslator;
import com.example.restservice.utils.TestUtils;
import java.util.List;
import java.util.Optional;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.mockito.ArgumentCaptor;

@ExtendWith(MockitoExtension.class)
class InvitationServiceTest {

    @InjectMocks
    private InvitationService invitationService;
    @Mock
    private InvitationRepository invitationRepository;
    @Mock
    private InvitationTranslator invitationTranslator;
    @Mock
    private ResourceService resourceService;
    @Mock
    private UserCRUDService userCRUDService;
    @Mock
    private AuthenticatedUserService authenticatedUserService;
    @Mock
    private MailService mailService;
    @Mock
    private PermissionService permissionService;

    @Mock
    private User fromUser;
    @Mock
    private User toUser;
    @Mock
    private InviteUserDto inviteUserDto;
    @Mock
    private Resource resource;
    @Mock
    private Invitation invitation;
    @Mock
    private Invitation expiredInvitation;
    @Mock
    private RequestAccessDto requestAccessDto;
    @Mock
    private PendingInvitationDto pendingInvitationDto;

    private TestUtils testUtils;

    private final PermissionMode PERMISSION_MODE = PermissionMode.READ_WRITE;

    private final static String AUTHENTICATED_USERNAME = "someUser";
    private final static String INVITATION_USERNAME = "anotherUser";
    private final static String TOKEN_ID = "tokenId";
    private final static long RESOURCE_ID = 123L;

    public InvitationServiceTest() {
        this.testUtils = new TestUtils();
    }

    @Test
    void invite() throws NotFoundException, UserWithoutPermissionsException {

        when(authenticatedUserService.getUsername()).thenReturn(AUTHENTICATED_USERNAME);
        when(inviteUserDto.getUsername()).thenReturn(INVITATION_USERNAME);

        when(userCRUDService.findByUsername(AUTHENTICATED_USERNAME)).thenReturn(fromUser);
        when(userCRUDService.findByUsername(INVITATION_USERNAME)).thenReturn(toUser);
        when(resourceService.getGrantedResource(RESOURCE_ID)).thenReturn(resource);
        when(inviteUserDto.getPermissionMode()).thenReturn(PERMISSION_MODE);
        when(inviteUserDto.getResourceId()).thenReturn(RESOURCE_ID);

        when(invitationTranslator.fromInvitationsDto(PERMISSION_MODE, fromUser, toUser, resource, InvitationType.SEND)).thenReturn(invitation);

        invitationService.invite(inviteUserDto);

        verify(invitationRepository).save(invitation);
        verify(mailService).sendInvitation(invitation);

        verifyNoMoreInteractions(invitationRepository);
        verifyNoMoreInteractions(mailService);
    }

    @Test
    void requestAccess_shouldThrowException_ifResourceIsNotPublic() throws NotFoundException {

        when(authenticatedUserService.getUsername()).thenReturn(AUTHENTICATED_USERNAME);
        when(userCRUDService.findByUsername(AUTHENTICATED_USERNAME)).thenReturn(fromUser);
        when(resourceService.getResource(RESOURCE_ID)).thenReturn(resource);
        when(requestAccessDto.getResourceId()).thenReturn(RESOURCE_ID);
        when(resource.getUser()).thenReturn(toUser);
        when(resource.getIsPublic()).thenReturn(false);

        assertThrows(UserWithoutPermissionsException.class, () -> invitationService.requestAccess(requestAccessDto));
    }

    @Test
    void requestAccess() throws NotFoundException, UserWithoutPermissionsException {

        when(authenticatedUserService.getUsername()).thenReturn(AUTHENTICATED_USERNAME);
        when(userCRUDService.findByUsername(AUTHENTICATED_USERNAME)).thenReturn(fromUser);
        when(resourceService.getResource(RESOURCE_ID)).thenReturn(resource);
        when(requestAccessDto.getResourceId()).thenReturn(RESOURCE_ID);
        when(resource.getUser()).thenReturn(toUser);
        when(resource.getIsPublic()).thenReturn(true);
        when(requestAccessDto.getPermissionMode()).thenReturn(PERMISSION_MODE);

        when(invitationTranslator.fromInvitationsDto(PERMISSION_MODE, fromUser, toUser, resource, InvitationType.ASK)).thenReturn(invitation);

        invitationService.requestAccess(requestAccessDto);

        verify(invitationRepository).save(invitation);
        verify(mailService).sendInvitation(invitation);

        verifyNoMoreInteractions(invitationRepository);
        verifyNoMoreInteractions(mailService);
    }

    @Test
    void acceptInvitation_shouldThrowException_whenTokenDoesntExists() throws NotFoundException {

        when(authenticatedUserService.getUsername()).thenReturn(AUTHENTICATED_USERNAME);
        when(userCRUDService.findByUsername(AUTHENTICATED_USERNAME)).thenReturn(toUser);

        when(invitationRepository.findByToUserAndToken(toUser, TOKEN_ID)).thenReturn(Optional.empty());

        assertThrows(InvalidTokenException.class, () -> invitationService.acceptInvitation(TOKEN_ID));
    }

    @Test
    void acceptInvitation_shouldThrowException_whenTokenIsExpired() throws NotFoundException {

        when(authenticatedUserService.getUsername()).thenReturn(AUTHENTICATED_USERNAME);
        when(userCRUDService.findByUsername(AUTHENTICATED_USERNAME)).thenReturn(toUser);

        when(invitationRepository.findByToUserAndToken(toUser, TOKEN_ID)).thenReturn(Optional.of(invitation));

        when(invitation.getExpiryDate()).thenReturn(testUtils.getOldDate());

        assertThrows(InvalidTokenException.class, () -> invitationService.acceptInvitation(TOKEN_ID));
    }

    @Test
    void acceptSendInvitation() throws NotFoundException, InvalidTokenException {

        when(authenticatedUserService.getUsername()).thenReturn(AUTHENTICATED_USERNAME);
        when(userCRUDService.findByUsername(AUTHENTICATED_USERNAME)).thenReturn(toUser);

        when(invitationRepository.findByToUserAndToken(toUser, TOKEN_ID)).thenReturn(Optional.of(invitation));

        when(invitation.getExpiryDate()).thenReturn(testUtils.getFutureDate());
        when(invitation.getPermissionMode()).thenReturn(PERMISSION_MODE);
        when(invitation.getToUser()).thenReturn(toUser);
        when(invitation.getResource()).thenReturn(resource);
        when(invitation.getInvitationType()).thenReturn(InvitationType.SEND);

        invitationService.acceptInvitation(TOKEN_ID);

        verify(permissionService).add(toUser, PERMISSION_MODE, resource);

        ArgumentCaptor<Invitation> invitationArgumentCaptor = ArgumentCaptor.forClass(Invitation.class);

        verify(invitationRepository).save(invitationArgumentCaptor.capture());

        Invitation acceptedInvitation = invitationArgumentCaptor.getValue();

        assertTrue(acceptedInvitation.getAccepted());
    }

    @Test
    void acceptAskInvitation() throws NotFoundException, InvalidTokenException {

        when(authenticatedUserService.getUsername()).thenReturn(AUTHENTICATED_USERNAME);
        when(userCRUDService.findByUsername(AUTHENTICATED_USERNAME)).thenReturn(toUser);

        when(invitationRepository.findByToUserAndToken(toUser, TOKEN_ID)).thenReturn(Optional.of(invitation));

        when(invitation.getExpiryDate()).thenReturn(testUtils.getFutureDate());
        when(invitation.getPermissionMode()).thenReturn(PERMISSION_MODE);
        when(invitation.getFromUser()).thenReturn(fromUser);
        when(invitation.getResource()).thenReturn(resource);
        when(invitation.getInvitationType()).thenReturn(InvitationType.ASK);

        invitationService.acceptInvitation(TOKEN_ID);

        verify(permissionService).add(fromUser, PERMISSION_MODE, resource);

        ArgumentCaptor<Invitation> invitationArgumentCaptor = ArgumentCaptor.forClass(Invitation.class);

        verify(invitationRepository).save(invitationArgumentCaptor.capture());

        Invitation acceptedInvitation = invitationArgumentCaptor.getValue();

        assertTrue(acceptedInvitation.getAccepted());
    }

    @Test
    void getPendingInvitations() throws NotFoundException {

        when(authenticatedUserService.getUsername()).thenReturn(AUTHENTICATED_USERNAME);
        when(userCRUDService.findByUsername(AUTHENTICATED_USERNAME)).thenReturn(toUser);

        when(invitationRepository.findByAcceptedAndToUser(false, toUser)).thenReturn(Lists.newArrayList(invitation, expiredInvitation));

        when(invitation.getExpiryDate()).thenReturn(testUtils.getFutureDate());
        when(expiredInvitation.getExpiryDate()).thenReturn(testUtils.getOldDate());

        when(invitationTranslator.toPendingInvitationDto(invitation)).thenReturn(pendingInvitationDto);

        List<PendingInvitationDto> pendingInvitationDtoList = invitationService.getPendingInvitations();

        assertEquals(1, pendingInvitationDtoList.size());
        assertEquals(pendingInvitationDto, pendingInvitationDtoList.get(0));
    }
}