package com.example.restservice.service;

import com.example.restservice.model.User;
import com.example.restservice.model.VerificationToken;
import com.example.restservice.repository.VerificationTokenRepository;
import com.example.restservice.utils.TestUtils;
import java.time.Duration;
import java.util.Date;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VerificationTokenServiceTest {

    @InjectMocks
    private VerificationTokenService verificationTokenService;
    @Mock
    private VerificationTokenRepository verificationTokenRepository;
    @Mock
    private VerificationToken returnedVerificationToken;
    @Mock
    private User user;
    @Mock
    private VerificationToken verificationToken;

    private TestUtils testUtils;

    private final static String TOKEN = "token";
    private final static int TOKEN_DURATION_HOURS = 24;

    public VerificationTokenServiceTest() {
        this.testUtils = new TestUtils();
    }

    @Test
    void create_shouldPersistAndReturnAValidToken() {

        ArgumentCaptor<VerificationToken> verificationTokenArgumentCaptor = ArgumentCaptor.forClass(VerificationToken.class);

        when(verificationTokenRepository.save(verificationTokenArgumentCaptor.capture())).thenReturn(returnedVerificationToken);

        VerificationToken verificationToken = verificationTokenService.create(user);

        verify(verificationTokenRepository).save(verificationTokenArgumentCaptor.capture());

        VerificationToken persistedVerificationToken = verificationTokenArgumentCaptor.getValue();

        assertEquals(user, persistedVerificationToken.getUser());

        long tokenDurationInMinutes =
            Duration.between(new Date().toInstant(), persistedVerificationToken.getExpiryDate().toInstant())
                    .toMinutes();

        int tokenDurationInHoursRoundedUp = Math.round(((float) tokenDurationInMinutes) / 60);

        assertEquals(TOKEN_DURATION_HOURS, tokenDurationInHoursRoundedUp);
        assertEquals(returnedVerificationToken, verificationToken);
    }

    @Test
    void isTokenValid_shouldReturnTrue_whenTokenExistsAndIsNotExpired() {

        when(verificationTokenRepository.findByTokenAndUser(TOKEN, user)).thenReturn(Optional.of(verificationToken));
        when(verificationToken.getExpiryDate()).thenReturn(testUtils.getFutureDate());

        assertTrue(verificationTokenService.isTokenValid(user, TOKEN));
    }

    @Test
    void isTokenValid_shouldReturnFalse_whenTokenExistsAndIsExpired() {

        when(verificationTokenRepository.findByTokenAndUser(TOKEN, user)).thenReturn(Optional.of(verificationToken));
        when(verificationToken.getExpiryDate()).thenReturn(testUtils.getOldDate());

        assertFalse(verificationTokenService.isTokenValid(user, TOKEN));
    }

    @Test
    void isTokenValid_shouldReturnFalse_whenTokenDoesntExist() {

        when(verificationTokenRepository.findByTokenAndUser(TOKEN, user)).thenReturn(Optional.empty());

        assertFalse(verificationTokenService.isTokenValid(user, TOKEN));
    }
}