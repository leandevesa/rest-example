package com.example.restservice.utils;

import java.util.Calendar;
import java.util.Date;
import org.springframework.stereotype.Service;

@Service
public class TestUtils {

    public Date getFutureDate() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

    public Date getOldDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, Calendar.JANUARY, 1);
        return calendar.getTime();
    }
}
