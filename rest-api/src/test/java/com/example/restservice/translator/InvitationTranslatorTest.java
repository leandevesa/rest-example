package com.example.restservice.translator;

import com.example.restservice.dto.invitation.PendingInvitationDto;
import com.example.restservice.model.Invitation;
import com.example.restservice.model.InvitationType;
import com.example.restservice.model.PermissionMode;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import java.time.Duration;
import java.util.Date;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InvitationTranslatorTest {

    @InjectMocks
    private InvitationTranslator invitationTranslator;
    @Mock
    private User fromUser;
    @Mock
    private User toUser;
    @Mock
    private Resource resource;
    @Mock
    private Invitation invitation;

    private final static PermissionMode PERMISSION_MODE = PermissionMode.READ;
    private final static InvitationType INVITATION_TYPE = InvitationType.SEND;
    private final static String USERNAME = "username";
    private final static String TOKEN = "token";
    private final static String RESOURCE_NAME = "resourceName";

    @Test
    void fromInvitationsDto() {

        Invitation translatedInvitation = invitationTranslator.fromInvitationsDto(PERMISSION_MODE, fromUser, toUser, resource, INVITATION_TYPE);

        assertEquals(PERMISSION_MODE, translatedInvitation.getPermissionMode());
        assertEquals(fromUser, translatedInvitation.getFromUser());
        assertEquals(toUser, translatedInvitation.getToUser());
        assertEquals(INVITATION_TYPE, translatedInvitation.getInvitationType());
        assertEquals(resource, translatedInvitation.getResource());
        assertFalse(translatedInvitation.getAccepted());
        assertNotNull(translatedInvitation.getToken());

        Date expiryDate = translatedInvitation.getExpiryDate();

        long invitationDurationInHours =
            Duration.between(new Date().toInstant(), expiryDate.toInstant()).toHours();

        int tokenDurationInDaysRoundedUp =
            Math.round((float) invitationDurationInHours / 24);

        assertEquals(7, tokenDurationInDaysRoundedUp);
    }

    @Test
    void toPendingInvitationDto() {

        when(invitation.getResource()).thenReturn(resource);
        when(resource.getName()).thenReturn(RESOURCE_NAME);
        when(invitation.getFromUser()).thenReturn(fromUser);
        when(fromUser.getUsername()).thenReturn(USERNAME);
        when(invitation.getToken()).thenReturn(TOKEN);

        PendingInvitationDto pendingInvitationDto = invitationTranslator.toPendingInvitationDto(invitation);

        assertEquals(USERNAME, pendingInvitationDto.getFromUserName());
        assertEquals(TOKEN, pendingInvitationDto.getInvitationToken());
        assertEquals(RESOURCE_NAME, pendingInvitationDto.getResourceName());
    }
}