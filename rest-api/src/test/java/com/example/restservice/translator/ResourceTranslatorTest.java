package com.example.restservice.translator;

import com.example.restservice.dto.resource.ResourceDto;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.exceptions.misusing.NotAMockException;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ResourceTranslatorTest {

    @InjectMocks
    private ResourceTranslator resourceTranslator;
    @Mock
    private Resource resource;
    @Mock
    private User user;

    private final static String USERNAME = "username";
    private final static String CONTENT_TYPE = "contentType";
    private final static String DESCRIPTION = "description";
    private final static String NAME = "name";
    private final static String FILE_NAME = "fileName";
    private final static boolean IS_PUBLIC = true;

    @Test
    void toResourceDto() {

        when(user.getUsername()).thenReturn(USERNAME);
        when(resource.getName()).thenReturn(NAME);
        when(resource.getUser()).thenReturn(user);
        when(resource.getContentType()).thenReturn(CONTENT_TYPE);
        when(resource.getDescription()).thenReturn(DESCRIPTION);
        when(resource.getFileName()).thenReturn(FILE_NAME);
        when(resource.getIsPublic()).thenReturn(IS_PUBLIC);

        ResourceDto resourceDto = resourceTranslator.toResourceDto(resource);

        assertEquals(CONTENT_TYPE, resourceDto.getContentType());
        assertEquals(DESCRIPTION, resourceDto.getDescription());
        assertEquals(FILE_NAME, resourceDto.getFileName());
        assertEquals(IS_PUBLIC, resourceDto.getIsPublic());
        assertEquals(NAME, resourceDto.getName());
        assertEquals(USERNAME, resourceDto.getUsername());
    }
}