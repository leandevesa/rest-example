package com.example.restservice.translator;

import com.example.restservice.dto.user.UserDto;
import com.example.restservice.dto.user.UserProfileDto;
import com.example.restservice.dto.user.UserRegisterRequestDto;
import com.example.restservice.model.User;
import com.example.restservice.model.UserProfile;
import com.example.restservice.model.UserProfileInterest;
import com.example.restservice.service.PasswordHashingService;
import com.example.restservice.utils.TestUtils;
import java.util.Date;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserTranslatorTest {

    @InjectMocks
    private UserTranslator userTranslator;
    @Mock
    private PasswordHashingService passwordHashingService;
    @Mock
    private UserRegisterRequestDto userRegisterRequestDto;
    @Mock
    private UserProfileDto userProfileDto;
    @Mock
    private User user;
    @Mock
    private UserProfile userProfile;
    @Mock
    private UserProfileInterest userProfileInterest;

    private final TestUtils testUtils;

    private final static String USERNAME = "username";
    private final static String EMAIL = "email";
    private final static String PASSWORD = "password";
    private final static String HASHED_PASSWORD = "hashedPassword";
    private final static String AVATAR = "avatar";
    private final static String NAME = "name";
    private final static String LASTNAME = "lastname";
    private final static String INTEREST = "interest";
    private final Date BIRTHDATE;

    public UserTranslatorTest() {
        this.testUtils = new TestUtils();
        BIRTHDATE = testUtils.getOldDate();
    }

    @Test
    void fromUserRegisterDto() {

        when(userRegisterRequestDto.getUsername()).thenReturn(USERNAME);
        when(userRegisterRequestDto.getEmail()).thenReturn(EMAIL);
        when(userRegisterRequestDto.getPassword()).thenReturn(PASSWORD);

        when(userRegisterRequestDto.getUserProfile()).thenReturn(userProfileDto);
        when(userProfileDto.getAvatar()).thenReturn(AVATAR);
        when(userProfileDto.getName()).thenReturn(NAME);
        when(userProfileDto.getLastname()).thenReturn(LASTNAME);
        when(userProfileDto.getBirthdate()).thenReturn(BIRTHDATE);
        when(userProfileDto.getInterests()).thenReturn(Lists.newArrayList(INTEREST));

        when(passwordHashingService.hash(PASSWORD)).thenReturn(HASHED_PASSWORD);

        User translatedUser = userTranslator.fromUserRegisterDto(userRegisterRequestDto);

        assertEquals(USERNAME, translatedUser.getUsername());
        assertEquals(HASHED_PASSWORD, translatedUser.getPassword());
        assertEquals(EMAIL, translatedUser.getEmail());
        assertFalse(translatedUser.isDeleted());
        assertFalse(translatedUser.isVerified());

        UserProfile translatedUserProfile = translatedUser.getUserProfile();

        assertEquals(AVATAR, translatedUserProfile.getAvatar());
        assertEquals(NAME, translatedUserProfile.getName());
        assertEquals(LASTNAME, translatedUserProfile.getLastname());
        assertEquals(BIRTHDATE, translatedUserProfile.getBirthdate());
        assertEquals(1, translatedUserProfile.getInterests().size());
        assertEquals(INTEREST, translatedUserProfile.getInterests().get(0).getInterest());
    }

    @Test
    void toUserDto() {
        when(user.getUsername()).thenReturn(USERNAME);
        when(user.getEmail()).thenReturn(EMAIL);

        when(user.getUserProfile()).thenReturn(userProfile);
        when(userProfile.getAvatar()).thenReturn(AVATAR);
        when(userProfile.getName()).thenReturn(NAME);
        when(userProfile.getLastname()).thenReturn(LASTNAME);
        when(userProfile.getBirthdate()).thenReturn(BIRTHDATE);
        when(userProfile.getInterests()).thenReturn(Lists.newArrayList(userProfileInterest));
        when(userProfileInterest.getInterest()).thenReturn(INTEREST);

        UserDto translatedUserDto = userTranslator.toUserDto(user);

        assertEquals(USERNAME, translatedUserDto.getUsername());
        assertEquals(EMAIL, translatedUserDto.getEmail());

        UserProfileDto translatedUserProfileDto = translatedUserDto.getUserProfile();

        assertEquals(AVATAR, translatedUserProfileDto.getAvatar());
        assertEquals(NAME, translatedUserProfileDto.getName());
        assertEquals(LASTNAME, translatedUserProfileDto.getLastname());
        assertEquals(BIRTHDATE, translatedUserProfileDto.getBirthdate());
        assertEquals(1, translatedUserProfileDto.getInterests().size());
        assertEquals(INTEREST, translatedUserProfileDto.getInterests().get(0));
    }
}