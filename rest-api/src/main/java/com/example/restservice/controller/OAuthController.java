package com.example.restservice.controller;

import com.example.restservice.dto.user.UserDto;
import com.example.restservice.dto.user.UserLoginResponseDto;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import com.example.restservice.service.OAuthService;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OAuthController {

    private OAuthService oAuthService;

    public OAuthController(OAuthService oAuthService) {
        this.oAuthService = oAuthService;
    }

    @GetMapping("loginSuccess")
    public UserLoginResponseDto oauthLogin() {
        return oAuthService.login();
    }
}
