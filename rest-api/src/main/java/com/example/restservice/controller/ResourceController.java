package com.example.restservice.controller;

import com.example.restservice.dto.GenericApiResponseDto;
import com.example.restservice.dto.resource.ResourceDto;
import com.example.restservice.dto.resource.ResourcePublicDto;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import com.example.restservice.model.Resource;
import com.example.restservice.service.AuthenticatedUserService;
import com.example.restservice.service.ResourceService;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ResourceController {

	private ResourceService resourceService;
	private AuthenticatedUserService authenticatedUserService;

	public ResourceController(ResourceService resourceService,
                              AuthenticatedUserService authenticatedUserService) {
		this.resourceService = resourceService;
		this.authenticatedUserService = authenticatedUserService;
	}

	@PutMapping(value = "resource", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResourceDto create(@RequestParam("file") MultipartFile uploadedFile,
							  @RequestParam("name") String resourceName,
							  @RequestParam("description") String description,
							  @RequestParam("isPublic") boolean isPublic,
							  HttpServletResponse response)
		throws IOException, NotFoundException {

		String username = authenticatedUserService.getUsername();
		byte[] file = uploadedFile.getBytes();
		String contentType = uploadedFile.getContentType();
		String fileName = uploadedFile.getOriginalFilename();

		ResourceDto resourceDto =
			resourceService.save(username, resourceName, description, isPublic, contentType, fileName, file);

		response.setStatus(HttpServletResponse.SC_CREATED);
		return resourceDto;
	}

	@PatchMapping(value = "resource", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public GenericApiResponseDto update(@RequestParam("file") MultipartFile uploadedFile,
										@RequestParam("resourceId") Long resourceId,
										HttpServletResponse response)
		throws IOException, NotFoundException, UserWithoutPermissionsException {

		String username = authenticatedUserService.getUsername();
		byte[] file = uploadedFile.getBytes();
		String contentType = uploadedFile.getContentType();
		String fileName = uploadedFile.getOriginalFilename();

		resourceService.update(username, resourceId, contentType, fileName, file);

		return new GenericApiResponseDto(String.format("Resource '%s' updated ok", resourceId),
			HttpServletResponse.SC_OK);
	}

	@DeleteMapping("resource")
	public GenericApiResponseDto delete(@RequestParam("resourceId") Long resourceId,
										HttpServletResponse response)
		throws IOException, NotFoundException, UserWithoutPermissionsException {

		String username = authenticatedUserService.getUsername();

		resourceService.delete(username, resourceId);

		return new GenericApiResponseDto(String.format("Resource '%s' deleted ok", resourceId),
			HttpServletResponse.SC_OK);
	}

	@DeleteMapping("resource/{resourceId}/permission/{usernameToRemove}")
	public GenericApiResponseDto removePermission(@PathVariable("resourceId") Long resourceId, String usernameToRemove,
												  HttpServletResponse response)
		throws IOException, NotFoundException, UserWithoutPermissionsException {

		String username = authenticatedUserService.getUsername();

		resourceService.removePermission(username, resourceId, usernameToRemove);

		return new GenericApiResponseDto(String.format("Resource '%s' permission removed from '%s' ok", resourceId, usernameToRemove),
			HttpServletResponse.SC_OK);
	}

	@GetMapping("resource")
	public List<ResourcePublicDto> getPublicResources(HttpServletResponse response) {
		return resourceService.getAllPublicResources();
	}

	@GetMapping("resource/{resourceId}")
	public FileSystemResource getResource(@PathVariable Long resourceId, HttpServletResponse response)
		throws NotFoundException, UserWithoutPermissionsException {

		Resource resource = resourceService.getGrantedResource(resourceId);
		String resourcePath = resourceService.getResourcePath(resourceId);

		response.setContentType(resource.getContentType());
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", resource.getFileName()));
		return new FileSystemResource(resourcePath);
	}
}
