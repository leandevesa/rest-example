package com.example.restservice.controller;

import com.example.restservice.dto.GenericApiResponseDto;
import com.example.restservice.dto.user.UserDto;
import com.example.restservice.dto.user.UserLoginRequestDto;
import com.example.restservice.dto.user.UserLoginResponseDto;
import com.example.restservice.dto.user.UserPublicProfileDto;
import com.example.restservice.dto.user.UserRegisterRequestDto;
import com.example.restservice.exception.InvalidCredentialsException;
import com.example.restservice.exception.InvalidTokenException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserNotVerifiedException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import com.example.restservice.exception.UsernameTakenApiException;
import com.example.restservice.exception.UsernameTakenException;
import com.example.restservice.service.AuthenticatedUserService;
import com.example.restservice.service.UserService;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	private UserService userService;
	private AuthenticatedUserService authenticatedUserService;

	public UserController(UserService userService,
						  AuthenticatedUserService authenticatedUserService) {
		this.userService = userService;
		this.authenticatedUserService = authenticatedUserService;
	}

	@PutMapping("user")
	public GenericApiResponseDto newUser(@RequestBody UserRegisterRequestDto userRegisterRequestDto, HttpServletResponse response)
		throws UsernameTakenApiException {

		try {
			userService.register(userRegisterRequestDto);
			response.setStatus(HttpServletResponse.SC_CREATED);
			return new GenericApiResponseDto(String.format("User '%s' created ok", userRegisterRequestDto.getUsername()),
				HttpServletResponse.SC_CREATED);
		} catch (UsernameTakenException e) {
			throw new UsernameTakenApiException(e.getMessage());
		}
	}

	@GetMapping("user/{username}")
	public UserDto get(@PathVariable("username") String username, HttpServletResponse response)
		throws NotFoundException, UserWithoutPermissionsException {

		if (!authenticatedUserService.isValidUser(username)) throw new UserWithoutPermissionsException();

		return userService.get(username);
	}

	@GetMapping("user")
	public List<UserPublicProfileDto> getPublicUserList(HttpServletResponse response) {
		String username = authenticatedUserService.getUsername();
		return userService.getPublicUserList(username);
	}

	@DeleteMapping("user/{username}")
	public GenericApiResponseDto delete(@PathVariable("username") String username, HttpServletResponse response) throws NotFoundException {
		userService.delete(username);
		return new GenericApiResponseDto(String.format("User '%s' deleted ok", username), HttpServletResponse.SC_OK);
	}

	@PostMapping("user/{username}/verify/{token}")
	public GenericApiResponseDto verify(@PathVariable("username") String username, @PathVariable("token") String token,
										HttpServletResponse response)
        throws InvalidTokenException, NotFoundException {

		userService.verify(username, token);
		return new GenericApiResponseDto("User verified ok", HttpServletResponse.SC_OK);
	}

	@PostMapping("user")
	public UserLoginResponseDto login(@RequestBody UserLoginRequestDto userLoginRequestDto, HttpServletResponse response)
		throws InvalidCredentialsException, NotFoundException, UserNotVerifiedException {

		return userService.login(userLoginRequestDto.getUsername(), userLoginRequestDto.getPassword());
	}

	@PatchMapping("user/{username}")
	public UserDto update(@RequestBody UserDto userDto, @PathVariable String username, HttpServletResponse response)
		throws NotFoundException, UserWithoutPermissionsException {

		if (!authenticatedUserService.isValidUser(username)) throw new UserWithoutPermissionsException();

		return userService.update(username, userDto);
	}
}
