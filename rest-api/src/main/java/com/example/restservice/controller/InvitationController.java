package com.example.restservice.controller;

import com.example.restservice.dto.GenericApiResponseDto;
import com.example.restservice.dto.invitation.PendingInvitationDto;
import com.example.restservice.dto.invitation.InviteUserDto;
import com.example.restservice.dto.invitation.RequestAccessDto;
import com.example.restservice.exception.InvalidTokenException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import com.example.restservice.service.InvitationService;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvitationController {

    private InvitationService invitationService;

    public InvitationController(InvitationService invitationService) {
        this.invitationService = invitationService;
    }

    @PostMapping("invitation/send")
    public GenericApiResponseDto inviteUser(@RequestBody InviteUserDto inviteUserDto,
                                            HttpServletResponse httpServletResponse)
        throws NotFoundException, UserWithoutPermissionsException {

        this.invitationService.invite(inviteUserDto);

        return new GenericApiResponseDto("Invitation sent ok", HttpServletResponse.SC_OK);
    }

    @PostMapping("invitation/request")
    public GenericApiResponseDto requestAccess(@RequestBody RequestAccessDto requestAccessDto,
                                               HttpServletResponse httpServletResponse)
        throws NotFoundException, UserWithoutPermissionsException {

        this.invitationService.requestAccess(requestAccessDto);

        return new GenericApiResponseDto("Request access sent ok", HttpServletResponse.SC_OK);
    }

    @PostMapping("invitation/accept/{tokenId}")
    public GenericApiResponseDto acceptInvitation(@PathVariable String tokenId,
                                                  HttpServletResponse httpServletResponse)
        throws NotFoundException, InvalidTokenException {

        this.invitationService.acceptInvitation(tokenId);

        return new GenericApiResponseDto("Invitation accepted ok", HttpServletResponse.SC_OK);
    }

    @GetMapping("invitation")
    public List<PendingInvitationDto> getPendingInvitations(HttpServletResponse httpServletResponse)
        throws NotFoundException {

        return this.invitationService.getPendingInvitations();
    }
}
