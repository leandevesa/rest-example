package com.example.restservice.service;

import com.example.restservice.exception.InvalidCredentialsException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserNotVerifiedException;
import java.util.Base64;
import org.springframework.stereotype.Service;

@Service
public class BasicAuthService {

    private UserService userService;

    public BasicAuthService(UserService userService) {
        this.userService = userService;
    }

    public String getValidUsername(String headerValue) throws NotFoundException, InvalidCredentialsException, UserNotVerifiedException {
        String[] userAndPassword = new String(Base64.getDecoder().decode(headerValue)).split(":");
        String username = userAndPassword[0];
        String password = userAndPassword[1];

        userService.login(username, password);

        return username;
    }
}
