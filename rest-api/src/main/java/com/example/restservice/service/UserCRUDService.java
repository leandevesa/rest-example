package com.example.restservice.service;

import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UsernameTakenException;
import com.example.restservice.model.User;
import com.example.restservice.repository.UserRepository;
import java.util.List;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class UserCRUDService {
    private UserRepository userRepository;

    public UserCRUDService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(User user) throws UsernameTakenException {
        try {
            return userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new UsernameTakenException(user.getUsername());
        }
    }

    public void update(User user) {
        userRepository.save(user);
    }

    public User findByUsername(String username) throws NotFoundException {
        return userRepository.findByUsernameAndDeletedFalse(username)
                             .orElseThrow(() -> new NotFoundException(String.format("User '%s' not found", username)));
    }

    public boolean exists(String username) {
        return userRepository.findByUsernameAndDeletedFalse(username).isPresent();
    }

    public List<User> findAll() {
        return userRepository.findByDeletedFalse();
    }
}
