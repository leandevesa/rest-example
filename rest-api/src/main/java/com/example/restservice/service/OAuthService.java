package com.example.restservice.service;

import com.example.restservice.dto.user.UserLoginResponseDto;
import com.example.restservice.model.User;
import com.example.restservice.model.UserProfile;
import java.util.Collections;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class OAuthService {

    private AuthenticatedUserService authenticatedUserService;
    private UserCRUDService userCRUDService;
    private JWTService jwtService;

    public OAuthService(AuthenticatedUserService authenticatedUserService,
                        UserCRUDService userCRUDService,
                        JWTService jwtService) {

        this.authenticatedUserService = authenticatedUserService;
        this.userCRUDService = userCRUDService;
        this.jwtService = jwtService;
    }

    public UserLoginResponseDto login() {

        User user = getUser();

        if (!userCRUDService.exists(user.getUsername())) {
            userCRUDService.save(user);
        }

        String jwtToken = jwtService.getJWTToken(user.getUsername());

        return UserLoginResponseDto.newBuilder()
                                   .withToken(jwtToken)
                                   .withUsername(user.getUsername())
                                   .build();
    }

    private User getUser() {
        Map<String, Object> userAttributes = this.authenticatedUserService.getOAuthClaims();

        String email = (String) userAttributes.get("email");
        String name = getName(userAttributes);
        String lastName = getLastName(userAttributes);
        String avatar = (String) userAttributes.get("picture");

        UserProfile userProfile = UserProfile.newBuilder()
                                             .withInterests(Collections.emptyList())
                                             .withName(name)
                                             .withLastname(lastName)
                                             .withAvatar(avatar)
                                             .build();

        return User.newBuilder()
                   .withUsername(email)
                   .withDeleted(false)
                   .withEmail(email)
                   .withVerified(true)
                   .withUserProfile(userProfile)
                   .build();
    }

    private String getName(Map<String, Object> userAttributes) {
        return userAttributes.get("given_name") != null ?
               (String) userAttributes.get("given_name") :
               ((String) userAttributes.get("name")).split(" ")[0];
    }

    private String getLastName(Map<String, Object> userAttributes) {
        return userAttributes.get("family_name") != null ?
               (String) userAttributes.get("family_name") :
               ((String) userAttributes.get("name")).split(" ")[1];
    }
}
