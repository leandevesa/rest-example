package com.example.restservice.service;

import com.example.restservice.dto.resource.ResourceDto;
import com.example.restservice.dto.resource.ResourcePublicDto;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import com.example.restservice.repository.ResourceRepository;
import com.example.restservice.translator.ResourceTranslator;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class ResourceService {

    private ResourceRepository resourceRepository;
    private FileService fileService;
    private UserCRUDService userCRUDService;
    private AuthenticatedUserService authenticatedUserService;
    private PermissionService permissionService;
    private ResourceTranslator resourceTranslator;

    public ResourceService(ResourceRepository resourceRepository,
                           FileService fileService,
                           UserCRUDService userCRUDService,
                           AuthenticatedUserService authenticatedUserService,
                           PermissionService permissionService,
                           ResourceTranslator resourceTranslator) {

        this.resourceRepository = resourceRepository;
        this.fileService = fileService;
        this.userCRUDService = userCRUDService;
        this.authenticatedUserService = authenticatedUserService;
        this.permissionService = permissionService;
        this.resourceTranslator = resourceTranslator;
    }

    @Transactional
    public ResourceDto save(String username, String resourceName, String description, boolean isPublic, String contentType,
                            String fileName, byte[] file)
        throws IOException, NotFoundException {

        Resource resource = persistResourceMetadata(username, resourceName, description, isPublic, contentType, fileName);
        fileService.saveToDisk(file, resource.getId());

        return resourceTranslator.toResourceDto(resource);
    }

    public void update(String username, Long resourceId, String contentType, String fileName, byte[] file)
        throws IOException, NotFoundException, UserWithoutPermissionsException {

        User user = userCRUDService.findByUsername(username);
        Resource resource = getResource(resourceId);

        if (!permissionService.hasWriteAccessToResource(user, resource)) throw new UserWithoutPermissionsException();

        Resource updatedResource = Resource.newBuilder(resource)
                                           .withContentType(contentType)
                                           .withFileName(fileName)
                                           .build();

        resourceRepository.save(updatedResource);
        fileService.saveToDisk(file, resource.getId());
    }

    public void delete(String username, Long resourceId)
        throws IOException, NotFoundException, UserWithoutPermissionsException {
        Resource resource = getResource(resourceId);

        if (!username.equalsIgnoreCase(resource.getUser().getUsername())) throw new UserWithoutPermissionsException();

        resourceRepository.delete(resource);
        fileService.deleteFromDisk(resource.getId());
    }

    public List<ResourcePublicDto> getAllPublicResources() {
        return resourceRepository.findByIsPublic(true).stream()
                                 .map(r -> new ResourcePublicDto(r.getId(), r.getName(), r.getDescription()))
                                 .collect(Collectors.toList());
    }

    public Resource getResource(Long resourceId) throws NotFoundException {

        return resourceRepository.findById(resourceId)
                                 .orElseThrow(() ->
                                                  new NotFoundException(String.format("Resource '%s' not found", resourceId.toString())));
    }

    public Resource getGrantedResource(Long resourceId) throws NotFoundException, UserWithoutPermissionsException {

        Resource resource = getResource(resourceId);

        if (!hasAccessToResource(resource)) throw new UserWithoutPermissionsException();

        return resource;
    }

    public void removePermission(String currentUser, Long resourceId, String usernameToRemove)
        throws NotFoundException, UserWithoutPermissionsException {

        Resource resource = getResource(resourceId);

        if (!currentUser.equalsIgnoreCase(resource.getUser().getUsername())) throw new UserWithoutPermissionsException();

        User userToRemove = userCRUDService.findByUsername(usernameToRemove);

        permissionService.remove(userToRemove, resource);
    }

    private boolean hasAccessToResource(Resource resource) throws NotFoundException {

        User user = userCRUDService.findByUsername(authenticatedUserService.getUsername());

        if (user.getUsername().equalsIgnoreCase(resource.getUser().getUsername())) return true;

        return permissionService.hasAccessToResource(user, resource);
    }

    public String getResourcePath(Long resourceId) {
        return fileService.getResourcePath(resourceId);
    }

    private Resource persistResourceMetadata(String username, String resourceName, String description, boolean isPublic,
                                             String contentType, String fileName)
        throws NotFoundException {

        User user = userCRUDService.findByUsername(username);

        Resource resource = Resource.newBuilder()
                                    .withUser(user)
                                    .withName(resourceName)
                                    .withDescription(description)
                                    .withIsPublic(isPublic)
                                    .withContentType(contentType)
                                    .withFileName(fileName)
                                    .build();

        return resourceRepository.save(resource);
    }
}
