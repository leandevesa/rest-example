package com.example.restservice.service;

import java.util.Properties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

@Service
public class MailSenderService {

    private final String host;
    private final String username;
    private final String password;
    private final int port;
    private final String protocol;
    private final boolean smtpAuth;
    private final boolean smtpTls;
    private final boolean debug;

    public MailSenderService(@Value("${mail.host}") String host,
                             @Value("${mail.port}") int port,
                             @Value("${mail.username}") String username,
                             @Value("${mail.password}") String password,
                             @Value("${mail.protocol}") String protocol,
                             @Value("${mail.smtp.auth}") boolean smtpAuth,
                             @Value("${mail.smtp.tls}") boolean smtpTls,
                             @Value("${mail.debug}") boolean debug) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.protocol = protocol;
        this.smtpAuth = smtpAuth;
        this.smtpTls = smtpTls;
        this.debug = debug;
    }

    // Motivos por los cual este metodo no es asincronico (y estoy penalizando al usuario con mayor tiempo de respuesta):
    //      Para que sea asincronico tengo que implementar alguna logica de reintentos de reenvio de mail, ya que si no, se
    //      podria perder la transaccionabilidad (el usuario queda creado en la db, y al mismo nunca le llega el mail de confir)
    //      y creo que esta funcionalidad, excede los objetivos del tp
    public void send(String to, String subject, String text) {
        JavaMailSender mailSender = createSender();
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@restapi.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        mailSender.send(message);
    }

    private JavaMailSender createSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", protocol);
        props.put("mail.smtp.auth", smtpAuth);
        props.put("mail.smtp.starttls.enable", smtpTls);
        props.put("mail.debug", debug);
        return mailSender;
    }
}
