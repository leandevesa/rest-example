package com.example.restservice.service;

import com.example.restservice.dto.user.UserDto;
import com.example.restservice.dto.user.UserLoginResponseDto;
import com.example.restservice.dto.user.UserPublicProfileDto;
import com.example.restservice.dto.user.UserRegisterRequestDto;
import com.example.restservice.exception.InvalidCredentialsException;
import com.example.restservice.exception.InvalidTokenException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserNotVerifiedException;
import com.example.restservice.exception.UsernameTakenException;
import com.example.restservice.model.User;
import com.example.restservice.model.VerificationToken;
import com.example.restservice.translator.UserTranslator;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserCRUDService userCRUDService;
    private UserTranslator userTranslator;
    private UserMergerService userMergerService;
    private VerificationTokenService verificationTokenService;
    private JWTService jwtService;
    private MailService mailService;
    private PasswordHashingService passwordHashingService;

    public UserService(UserCRUDService userCRUDService, UserTranslator userTranslator,
                       VerificationTokenService verificationTokenService,
                       UserMergerService userMergerService,
                       MailService mailService,
                       JWTService jwtService,
                       PasswordHashingService passwordHashingService) {

        this.userCRUDService = userCRUDService;
        this.userTranslator = userTranslator;
        this.userMergerService = userMergerService;
        this.verificationTokenService = verificationTokenService;
        this.jwtService = jwtService;
        this.mailService = mailService;
        this.passwordHashingService = passwordHashingService;
    }

    @Transactional
    public void register(UserRegisterRequestDto userRegisterRequestDto) throws UsernameTakenException {
        User domainUser = userTranslator.fromUserRegisterDto(userRegisterRequestDto);
        User persistedUser = userCRUDService.save(domainUser);
        VerificationToken verificationToken = verificationTokenService.create(persistedUser);
        mailService.sendVerificationToken(verificationToken);
    }

    public void verify(String username, String token) throws NotFoundException, InvalidTokenException {
        User user = userCRUDService.findByUsername(username);

        if (!verificationTokenService.isTokenValid(user, token)) throw new InvalidTokenException();

        User verifiedUser = User.newBuilder(user)
                                .withVerified(true)
                                .build();

        userCRUDService.update(verifiedUser);
    }

    public UserDto get(String username) throws NotFoundException {
        User user = userCRUDService.findByUsername(username);
        return userTranslator.toUserDto(user);
    }

    public List<UserPublicProfileDto> getPublicUserList(String username) {
        return userCRUDService.findAll().stream()
                              .filter(u -> !u.getUsername().equalsIgnoreCase(username))
                              .map(User::getUserProfile)
                              .map(p -> new UserPublicProfileDto(p.getName(), p.getLastname(), p.getAvatar()))
                              .collect(Collectors.toList());
    }

    public void delete(String username) throws NotFoundException {

        // Se procede a un borrado logico ya que no creo que este bien hacer un borrado fisico de registros
        // de usuarios en una base transaccional. Eventualmente se podria proceder a purgar la tabla por el deleted=false
        // si se lo desea
        // Esto conlleva a que nombres de usuarios queden "tomados" hasta su eventual purga, lo cual me parece ok
        // Por otra parte, el userName esta declarado como un uniqueconstraint y usandolo como un id, lo cual esto hace que tome mas fuerza
        // en el borrado logico

        User user = userCRUDService.findByUsername(username);

        User deletedUser = User.newBuilder(user)
                               .withDeleted(true)
                               .build();

        userCRUDService.update(deletedUser);
    }

    public UserLoginResponseDto login(String username, String password)
        throws NotFoundException, InvalidCredentialsException, UserNotVerifiedException {

        User persistedUser = userCRUDService.findByUsername(username);

        if (!passwordHashingService.passwordsMatch(password, persistedUser.getPassword())) throw new InvalidCredentialsException();
        if (!persistedUser.isVerified()) throw new UserNotVerifiedException();

        String jwtToken = jwtService.getJWTToken(username);

        return UserLoginResponseDto.newBuilder()
                                   .withToken(jwtToken)
                                   .withUsername(username)
                                   .build();
    }

    public UserDto update(String username, UserDto newUserValuesDto) throws NotFoundException {

        User persistedUser = userCRUDService.findByUsername(username);
        User modifiedUser = userTranslator.fromUserDto(newUserValuesDto, false);

        User newUser = userMergerService.merge(persistedUser, modifiedUser);

        userCRUDService.update(newUser);

        return userTranslator.toUserDto(newUser);
    }
}
