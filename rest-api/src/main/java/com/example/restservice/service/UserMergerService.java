package com.example.restservice.service;

import com.example.restservice.model.User;
import com.example.restservice.model.UserProfile;
import com.example.restservice.model.UserProfileInterest;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class UserMergerService {

    public User merge(User oldUser, User modifiedUser) {

        UserProfile oldUserProfile = oldUser.getUserProfile();
        UserProfile modifiedUserProfile = modifiedUser.getUserProfile();

        UserProfile mergedUserProfile = getMergedProfile(oldUserProfile, modifiedUserProfile);
        String modifiedEmail = (modifiedUser.getEmail() != null ? modifiedUser.getEmail() : oldUser.getEmail());

        return User.newBuilder(oldUser)
                   .withEmail(modifiedEmail)
                   .withUserProfile(mergedUserProfile)
                   .build();
    }

    private UserProfile getMergedProfile(UserProfile oldUserProfile, UserProfile modifiedUserProfile) {

        String name = (modifiedUserProfile.getName() != null ? modifiedUserProfile.getName() : oldUserProfile.getName());
        String lastname = (modifiedUserProfile.getLastname() != null ? modifiedUserProfile.getLastname() : oldUserProfile.getLastname());
        String avatar = (modifiedUserProfile.getAvatar() != null ? modifiedUserProfile.getAvatar() : oldUserProfile.getAvatar());
        Date birthdate = (modifiedUserProfile.getBirthdate() != null ? modifiedUserProfile.getBirthdate() : oldUserProfile.getBirthdate());
        List<UserProfileInterest> interests = (modifiedUserProfile.getInterests().size() > 0 ?
                                               modifiedUserProfile.getInterests() :
                                               oldUserProfile.getInterests());

        return UserProfile.newBuilder(oldUserProfile)
                          .withAvatar(avatar)
                          .withBirthdate(birthdate)
                          .withName(name)
                          .withLastname(lastname)
                          .withInterests(interests)
                          .build();
    }
}
