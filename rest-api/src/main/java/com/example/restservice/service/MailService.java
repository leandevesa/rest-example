package com.example.restservice.service;

import com.example.restservice.model.Invitation;
import com.example.restservice.model.InvitationType;
import com.example.restservice.model.PermissionMode;
import com.example.restservice.model.VerificationToken;
import org.springframework.stereotype.Service;

@Service
public class MailService {

    private MailSenderService mailSenderService;

    public MailService(MailSenderService mailSenderService) {
        this.mailSenderService = mailSenderService;
    }

    public void sendInvitation(Invitation invitation) {
        String resourceName = invitation.getResource().getName();
        String fromUsername = invitation.getFromUser().getUsername();
        String to = invitation.getToUser().getEmail();
        String subject = getInvitationSubject(invitation.getInvitationType(), fromUsername, resourceName);
        String text = getInvitationText(invitation.getInvitationType(), fromUsername, resourceName,
            invitation.getPermissionMode(), invitation.getToken());

        mailSenderService.send(to, subject, text);
    }

    // Aca podria haber usado directamente mapas, pero no me gusta la forma de inicializacion que tiene java 8.
    // Tambien podria haber instalado la dependencia de guava, pero como solamente la voy a usar aca, me parecio un overkill.
    // Opte por la mas simple y determinar los textos mediante un fn.

    private String getInvitationSubject(InvitationType invitationType, String fromUsername, String resourceName) {
        if (InvitationType.SEND.equals(invitationType)) {
            return String.format("%s has invited you to access file: %s", fromUsername, resourceName);
        } else {
            return String.format("%s is requesting access to file: %s", fromUsername, resourceName);
        }
    }

    private String getInvitationText(InvitationType invitationType, String fromUsername, String resourceName,
                                     PermissionMode permissionMode, String token) {

        if (InvitationType.SEND.equals(invitationType)) {
            return String.format("You have been invited by %s to access file: %s (%s). You have 7 days to accept invitation, by using the "
                + "following token: %s", fromUsername, resourceName, permissionMode.toString(), token);
        } else {
            return String.format("%s has asked to access file: %s (%s). You have 7 days to accept/decline the request, by using the "
                + "following token: %s", fromUsername, resourceName, permissionMode.toString(), token);
        }
    }

    public void sendVerificationToken(VerificationToken verificationToken) {
        String to = verificationToken.getUser().getEmail();
        String subject = "Account verification";
        String text = String.format(
            "Use this token: %s to verify '%s' account", verificationToken.getToken(), verificationToken.getUser().getUsername());

        mailSenderService.send(to, subject, text);
    }
}
