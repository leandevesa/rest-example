package com.example.restservice.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.springframework.stereotype.Service;

@Service
public class FileService {

    private final static String BASE_DIR = String.format("%s/uploaded-files/", System.getProperty("user.dir")) + "/";

    public void saveToDisk(byte[] file, Long fileId) throws IOException {
        Files.createDirectories(Paths.get(BASE_DIR));
        Files.write(Paths.get(BASE_DIR + fileId.toString()), file);
    }

    public void deleteFromDisk(Long fileId) throws IOException {
        Files.delete(Paths.get(BASE_DIR + fileId.toString()));
    }

    public String getResourcePath(Long fileId) {
        return BASE_DIR + fileId.toString();
    }
}
