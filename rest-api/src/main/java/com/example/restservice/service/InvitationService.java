package com.example.restservice.service;

import com.example.restservice.dto.invitation.PendingInvitationDto;
import com.example.restservice.dto.invitation.InviteUserDto;
import com.example.restservice.dto.invitation.RequestAccessDto;
import com.example.restservice.exception.InvalidTokenException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserWithoutPermissionsException;
import com.example.restservice.model.Invitation;
import com.example.restservice.model.InvitationType;
import com.example.restservice.model.PermissionMode;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import com.example.restservice.repository.InvitationRepository;
import com.example.restservice.translator.InvitationTranslator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class InvitationService {

    private InvitationRepository invitationRepository;
    private InvitationTranslator invitationTranslator;
    private ResourceService resourceService;
    private UserCRUDService userCRUDService;
    private AuthenticatedUserService authenticatedUserService;
    private MailService mailService;
    private PermissionService permissionService;

    public InvitationService(InvitationRepository invitationRepository,
                             InvitationTranslator invitationTranslator,
                             ResourceService resourceService,
                             UserCRUDService userCRUDService,
                             AuthenticatedUserService authenticatedUserService,
                             MailService mailService,
                             PermissionService permissionService) {

        this.invitationRepository = invitationRepository;
        this.invitationTranslator = invitationTranslator;
        this.resourceService = resourceService;
        this.userCRUDService = userCRUDService;
        this.authenticatedUserService = authenticatedUserService;
        this.mailService = mailService;
        this.permissionService = permissionService;
    }

    public void invite(InviteUserDto inviteUserDto)
        throws NotFoundException, UserWithoutPermissionsException {

        User fromUser = userCRUDService.findByUsername(authenticatedUserService.getUsername());
        Resource resource = resourceService.getGrantedResource(inviteUserDto.getResourceId());
        User toUser = userCRUDService.findByUsername(inviteUserDto.getUsername());

        performInvitation(inviteUserDto.getPermissionMode(), fromUser, toUser, resource, InvitationType.SEND);
    }

    public void requestAccess(RequestAccessDto requestAccessDto)
        throws NotFoundException, UserWithoutPermissionsException {

        User fromUser = userCRUDService.findByUsername(authenticatedUserService.getUsername());
        Resource resource = resourceService.getResource(requestAccessDto.getResourceId());
        User toUser = resource.getUser();

        if (!resource.getIsPublic()) throw new UserWithoutPermissionsException();

        performInvitation(requestAccessDto.getPermissionMode(), fromUser, toUser, resource, InvitationType.ASK);
    }

    private void performInvitation(PermissionMode permissionMode, User fromUser, User toUser,
                                   Resource resource, InvitationType invitationType) {

        Invitation invitation = invitationTranslator.fromInvitationsDto(permissionMode, fromUser, toUser, resource, invitationType);
        invitationRepository.save(invitation);
        mailService.sendInvitation(invitation);
    }

    public void acceptInvitation(String tokenId) throws NotFoundException, InvalidTokenException {

        Invitation invitation = retrieveValidInvitation(tokenId);

        User userToPermit = InvitationType.SEND.equals(invitation.getInvitationType()) ?
                                invitation.getToUser() :
                                invitation.getFromUser();

        permissionService.add(userToPermit, invitation.getPermissionMode(), invitation.getResource());

        Invitation acceptedInvitation = Invitation.newBuilder(invitation)
                                                  .withAccepted(true)
                                                  .build();

        invitationRepository.save(acceptedInvitation);
    }

    public List<PendingInvitationDto> getPendingInvitations() throws NotFoundException {

        User toUser = userCRUDService.findByUsername(authenticatedUserService.getUsername());
        List<Invitation> pendingInvitations = invitationRepository.findByAcceptedAndToUser(false, toUser);
        Date now = new Date();

        return pendingInvitations.stream()
                                 .filter(i -> i.getExpiryDate().after(now))
                                 .map(invitationTranslator::toPendingInvitationDto)
                                 .collect(Collectors.toList());
    }

    private Invitation retrieveValidInvitation(String tokenId) throws NotFoundException, InvalidTokenException {

        User toUser = userCRUDService.findByUsername(authenticatedUserService.getUsername());

        Invitation invitation = invitationRepository.findByToUserAndToken(toUser, tokenId)
                                                    .orElseThrow(InvalidTokenException::new);

        if (invitation.getExpiryDate().before(new Date())) throw new InvalidTokenException();

        return invitation;
    }
}
