package com.example.restservice.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

@Service
public class JWTService {

    private final static String KEY = "aSecretKey";
    private final static int ONE_MINUTE = 60000;

    public String getJWTToken(String username) {

        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
            .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
            .builder()
            .setId("login-rest")
            .setSubject(username)
            .claim("authorities",
                grantedAuthorities.stream()
                                  .map(GrantedAuthority::getAuthority)
                                  .collect(Collectors.toList()))
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + (ONE_MINUTE * 60)))
            .signWith(SignatureAlgorithm.HS512,
                KEY.getBytes()).compact();

        return "Bearer " + token;
    }

    public Claims validateToken(String jwtToken) {
        return Jwts.parser().setSigningKey(KEY.getBytes()).parseClaimsJws(jwtToken).getBody();
    }
}
