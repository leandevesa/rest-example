package com.example.restservice.service;

import com.example.restservice.exception.NotFoundException;
import com.example.restservice.model.Permission;
import com.example.restservice.model.PermissionMode;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import com.example.restservice.repository.PermissionRepository;
import org.springframework.stereotype.Service;

@Service
public class PermissionService {

    private PermissionRepository permissionRepository;

    public PermissionService(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    public void add(User user, PermissionMode permissionMode, Resource resource) {

        Permission permission = Permission.newBuilder()
                                          .withUser(user)
                                          .withPermissionMode(permissionMode)
                                          .withResource(resource)
                                          .build();

        this.permissionRepository.save(permission);
    }

    public void remove(User user, Resource resource) throws NotFoundException {

        Permission permission = permissionRepository.findByUserAndResource(user, resource)
                                                    .orElseThrow(() -> new NotFoundException("Permission not found"));

        this.permissionRepository.delete(permission);
    }

    public boolean hasAccessToResource(User user, Resource resource) {
        return permissionRepository.findByUserAndResource(user, resource).isPresent();
    }

    public boolean hasWriteAccessToResource(User user, Resource resource) {
        return permissionRepository.findByUserAndResource(user, resource)
                                   .map(r -> PermissionMode.READ_WRITE.equals(r.getPermissionMode()))
                                   .orElse(false);
    }
}
