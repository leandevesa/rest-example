package com.example.restservice.service;

import com.example.restservice.model.User;
import com.example.restservice.model.VerificationToken;
import com.example.restservice.repository.VerificationTokenRepository;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public class VerificationTokenService {

    private static final int EXPIRATION = 60 * 24;

    private VerificationTokenRepository verificationTokenRepository;

    public VerificationTokenService(VerificationTokenRepository verificationTokenRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
    }

    public VerificationToken create(User user) {
        VerificationToken verificationToken = createToken(user);
        return verificationTokenRepository.save(verificationToken);
    }

    public boolean isTokenValid(User user, String token) {
        return verificationTokenRepository.findByTokenAndUser(token, user)
                                          .filter(t -> t.getExpiryDate().after(new Date()))
                                          .isPresent();
    }

    private VerificationToken createToken(User user) {
        String token = UUID.randomUUID().toString();
        Date expiryDate = calculateExpiryDate();

        return VerificationToken.newBuilder()
                                .withUser(user)
                                .withToken(token)
                                .withExpiryDate(expiryDate)
                                .build();
    }

    private Date calculateExpiryDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, EXPIRATION);
        return new Date(cal.getTime().getTime());
    }
}
