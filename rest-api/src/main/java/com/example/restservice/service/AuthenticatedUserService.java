package com.example.restservice.service;

import java.util.Map;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Service;

@Service
public class AuthenticatedUserService {

    private final static String ROLE_ADMIN = "ROLE_ADMIN";

    public boolean isValidUser(String currentUsername) {
        String authenticatedUsername = getUsername();
        return (authenticatedUsername.equalsIgnoreCase(currentUsername)) || isAdmin();
    }

    private boolean isAdmin() {
        return SecurityContextHolder.getContext()
                                    .getAuthentication()
                                    .getAuthorities().stream()
                                    .anyMatch(a -> a.toString().equals(ROLE_ADMIN));
    }

    public String getUsername() {
        return (String) SecurityContextHolder.getContext()
                                             .getAuthentication()
                                             .getPrincipal();
    }

    public Map<String, Object> getOAuthClaims() {
        if (SecurityContextHolder.getContext().getAuthentication().getClass().getName().contains("OAuth2AuthenticationToken")) {
            return ((DefaultOAuth2User) SecurityContextHolder.getContext()
                                                             .getAuthentication()
                                                             .getPrincipal()).getAttributes();
        } else {
            return ((DefaultOidcUser) SecurityContextHolder.getContext()
                                                           .getAuthentication()
                                                           .getPrincipal()).getClaims();
        }
    }

    public boolean isAuthenticated() {
        return SecurityContextHolder.getContext() != null &&
               SecurityContextHolder.getContext()
                                    .getAuthentication() != null &&
               SecurityContextHolder.getContext()
                                    .getAuthentication()
                                    .getPrincipal() != null;
    }
}
