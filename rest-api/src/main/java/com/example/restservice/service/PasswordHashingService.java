package com.example.restservice.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordHashingService {

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public PasswordHashingService() {
        this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
    }

    public String hash(String password) {
        return bCryptPasswordEncoder.encode(password);
    }

    public boolean passwordsMatch(String enteredPassword, String persistedPassword) {
        return bCryptPasswordEncoder.matches(enteredPassword, persistedPassword);
    }
}
