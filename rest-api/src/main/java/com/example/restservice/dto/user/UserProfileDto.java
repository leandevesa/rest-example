package com.example.restservice.dto.user;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.springframework.util.CollectionUtils;

public class UserProfileDto {

    private String name;
    private String lastname;
    private String avatar;
    // TODO: Fix date
    private Date birthdate;
    private List<String> interests = Collections.emptyList();

    public UserProfileDto() {

    }

    private UserProfileDto(Builder builder) {
        name = builder.name;
        lastname = builder.lastname;
        avatar = builder.avatar;
        birthdate = builder.birthdate;
        interests = builder.interests;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(UserProfileDto copy) {
        Builder builder = new Builder();
        builder.name = copy.getName();
        builder.lastname = copy.getLastname();
        builder.avatar = copy.getAvatar();
        builder.birthdate = copy.getBirthdate();
        builder.interests = copy.getInterests();
        return builder;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAvatar() {
        return avatar;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public List<String> getInterests() {
        return interests;
    }

    public static final class Builder {

        private String name;
        private String lastname;
        private String avatar;
        private Date birthdate;
        private List<String> interests = Collections.emptyList();

        private Builder() {}

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withLastname(String lastname) {
            this.lastname = lastname;
            return this;
        }

        public Builder withAvatar(String avatar) {
            this.avatar = avatar;
            return this;
        }

        public Builder withBirthdate(Date birthdate) {
            this.birthdate = birthdate;
            return this;
        }

        public Builder withInterests(List<String> interests) {
            this.interests = interests;
            return this;
        }

        public UserProfileDto build() {
            return new UserProfileDto(this);
        }
    }
}
