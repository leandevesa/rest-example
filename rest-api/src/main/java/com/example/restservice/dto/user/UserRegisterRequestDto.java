package com.example.restservice.dto.user;

public class UserRegisterRequestDto extends UserDto {

    private String password;

    public UserRegisterRequestDto() {

    }

    public String getPassword() {
        return password;
    }
}
