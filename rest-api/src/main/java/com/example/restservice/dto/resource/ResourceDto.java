package com.example.restservice.dto.resource;


public class ResourceDto {

    private Long id;
    private String username;
    private String name;
    private String description;
    private String contentType;
    private String fileName;
    private boolean isPublic;

    private ResourceDto(Builder builder) {
        id = builder.id;
        username = builder.username;
        name = builder.name;
        description = builder.description;
        contentType = builder.contentType;
        fileName = builder.fileName;
        isPublic = builder.isPublic;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getContentType() {
        return contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean getIsPublic() {
        return isPublic;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ResourceDto copy) {
        Builder builder = new Builder();
        builder.id = copy.getId();
        builder.username = copy.getUsername();
        builder.name = copy.getName();
        builder.description = copy.getDescription();
        builder.contentType = copy.getContentType();
        builder.fileName = copy.getFileName();
        builder.isPublic = copy.getIsPublic();
        return builder;
    }

    public static final class Builder {

        private Long id;
        private String username;
        private String name;
        private String description;
        private String contentType;
        private String fileName;
        private boolean isPublic;

        private Builder() {}

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Builder withFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public Builder withIsPublic(boolean isPublic) {
            this.isPublic = isPublic;
            return this;
        }

        public ResourceDto build() {
            return new ResourceDto(this);
        }
    }
}
