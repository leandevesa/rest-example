package com.example.restservice.dto.user;

public class UserDto {

    private String username;
    private String email;
    private UserProfileDto userProfile;

    public UserDto() {

    }

    private UserDto(Builder builder) {
        username = builder.username;
        email = builder.email;
        userProfile = builder.userProfile;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(UserDto copy) {
        Builder builder = new Builder();
        builder.username = copy.getUsername();
        builder.email = copy.getEmail();
        builder.userProfile = copy.getUserProfile();
        return builder;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public UserProfileDto getUserProfile() {
        return userProfile;
    }

    public static final class Builder {

        private String username;
        private String email;
        private UserProfileDto userProfile;

        private Builder() {}

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withUserProfile(UserProfileDto userProfile) {
            this.userProfile = userProfile;
            return this;
        }

        public UserDto build() {
            return new UserDto(this);
        }
    }
}
