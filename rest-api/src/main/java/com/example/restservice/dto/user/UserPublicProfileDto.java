package com.example.restservice.dto.user;

import java.util.Date;
import java.util.List;

public class UserPublicProfileDto {

    private String name;
    private String lastname;
    private String avatar;

    public UserPublicProfileDto(String name, String lastname, String avatar) {
        this.name = name;
        this.lastname = lastname;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAvatar() {
        return avatar;
    }
}
