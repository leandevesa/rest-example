package com.example.restservice.dto.invitation;

import com.example.restservice.model.PermissionMode;

public class RequestAccessDto {

    private Long resourceId;
    private PermissionMode permissionMode;

    public RequestAccessDto() {

    }

    public Long getResourceId() {
        return resourceId;
    }

    public PermissionMode getPermissionMode() {
        return permissionMode;
    }
}
