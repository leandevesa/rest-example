package com.example.restservice.dto.invitation;

import com.example.restservice.model.InvitationType;

public class PendingInvitationDto {

    private String fromUserName;
    private String resourceName;
    private String invitationToken;
    private InvitationType invitationType;

    public PendingInvitationDto(String fromUserName, String resourceName,
                                String invitationToken, InvitationType invitationType) {
        this.fromUserName = fromUserName;
        this.resourceName = resourceName;
        this.invitationToken = invitationToken;
        this.invitationType = invitationType;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getInvitationToken() {
        return invitationToken;
    }

    public InvitationType getInvitationType() {
        return invitationType;
    }
}
