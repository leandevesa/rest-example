package com.example.restservice.dto.user;

public class UserLoginRequestDto {

	private String username;
	private String password;

	public UserLoginRequestDto() {

	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

}
