package com.example.restservice.dto.invitation;

import com.example.restservice.model.PermissionMode;

public class InviteUserDto {

    private Long resourceId;
    private String username;
    private PermissionMode permissionMode;

    public InviteUserDto() {

    }

    public Long getResourceId() {
        return resourceId;
    }

    public String getUsername() {
        return username;
    }

    public PermissionMode getPermissionMode() {
        return permissionMode;
    }
}
