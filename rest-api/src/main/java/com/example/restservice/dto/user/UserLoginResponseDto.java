package com.example.restservice.dto.user;

public class UserLoginResponseDto {

	private String username;
	private String token;

	private UserLoginResponseDto(Builder builder) {
		username = builder.username;
		token = builder.token;
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public static Builder newBuilder(UserLoginResponseDto copy) {
		Builder builder = new Builder();
		builder.username = copy.getUsername();
		builder.token = copy.getToken();
		return builder;
	}

	public String getUsername() {
		return username;
	}

	public String getToken() {
		return token;
	}

	public static final class Builder {

		private String username;
		private String token;

		private Builder() {}

		public Builder withUsername(String username) {
			this.username = username;
			return this;
		}

		public Builder withToken(String token) {
			this.token = token;
			return this;
		}

		public UserLoginResponseDto build() {
			return new UserLoginResponseDto(this);
		}
	}
}
