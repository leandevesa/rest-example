package com.example.restservice.dto;

public class GenericApiResponseDto {

    private String message;
    private int status;

    public GenericApiResponseDto(String message, int status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }
}
