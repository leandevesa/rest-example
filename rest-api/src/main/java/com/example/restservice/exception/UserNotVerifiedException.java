package com.example.restservice.exception;

import javax.servlet.http.HttpServletResponse;

public class UserNotVerifiedException extends ApiException {

    public UserNotVerifiedException() {
        super("User is not verified", HttpServletResponse.SC_BAD_REQUEST);
    }
}