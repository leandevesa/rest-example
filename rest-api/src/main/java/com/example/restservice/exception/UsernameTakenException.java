package com.example.restservice.exception;

public class UsernameTakenException extends RuntimeException {

    public UsernameTakenException(String username) {
        super(String.format("User '%s' is already taken", username));
    }
}