package com.example.restservice.exception;

import javax.servlet.http.HttpServletResponse;

public class NotFoundException extends ApiException {

    public NotFoundException(String message) {
        super(message, HttpServletResponse.SC_NOT_FOUND);
    }
}