package com.example.restservice.exception;

import javax.servlet.http.HttpServletResponse;

public class UserWithoutPermissionsException extends ApiException {

    public UserWithoutPermissionsException() {
        super("User doesnt have permissions to make this action", HttpServletResponse.SC_FORBIDDEN);
    }
}