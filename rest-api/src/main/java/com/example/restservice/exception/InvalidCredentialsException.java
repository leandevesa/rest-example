package com.example.restservice.exception;

import javax.servlet.http.HttpServletResponse;

public class InvalidCredentialsException extends ApiException {

    public InvalidCredentialsException() {
        super("User and password mismatch", HttpServletResponse.SC_FORBIDDEN);
    }
}