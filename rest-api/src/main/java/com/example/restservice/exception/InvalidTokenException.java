package com.example.restservice.exception;

import javax.servlet.http.HttpServletResponse;

public class InvalidTokenException extends ApiException {

    public InvalidTokenException() {
        super("Token is either expired or invalid", HttpServletResponse.SC_BAD_REQUEST);
    }
}