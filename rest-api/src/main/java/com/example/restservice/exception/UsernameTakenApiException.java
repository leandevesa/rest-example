package com.example.restservice.exception;

import javax.servlet.http.HttpServletResponse;

public class UsernameTakenApiException extends ApiException {

    public UsernameTakenApiException(String message) {
        super(message, HttpServletResponse.SC_BAD_REQUEST);
    }
}