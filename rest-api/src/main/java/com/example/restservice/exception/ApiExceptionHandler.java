package com.example.restservice.exception;

import com.example.restservice.dto.GenericApiResponseDto;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<GenericApiResponseDto> apiExceptionHandler(HttpServletRequest req, ApiException ex) {

        GenericApiResponseDto genericApiResponseDto =
            new GenericApiResponseDto(ex.getMessage(), ex.getStatusCode());

        return buildResponseEntity(genericApiResponseDto);
    }

    private ResponseEntity<GenericApiResponseDto> buildResponseEntity(GenericApiResponseDto genericApiResponseDto) {
        return ResponseEntity.status(genericApiResponseDto.getStatus()).body(genericApiResponseDto);
    }
}