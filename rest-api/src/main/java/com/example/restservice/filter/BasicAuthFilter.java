package com.example.restservice.filter;

import com.example.restservice.dto.GenericApiResponseDto;
import com.example.restservice.exception.InvalidCredentialsException;
import com.example.restservice.service.AuthenticatedUserService;
import com.example.restservice.service.BasicAuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

@Service
public class BasicAuthFilter extends OncePerRequestFilter {

    private BasicAuthService basicAuthService;
    private AuthenticatedUserService authenticatedUserService;
    private final String HEADER = "Authorization";
    private final String PREFIX = "Basic ";

    public BasicAuthFilter(BasicAuthService basicAuthService,
                           AuthenticatedUserService authenticatedUserService) {
        this.basicAuthService = basicAuthService;
        this.authenticatedUserService = authenticatedUserService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
        throws IOException {
        try {
            if (!authenticatedUserService.isAuthenticated()) {
                if (hasTokenHeader(request)) {
                    String username = basicAuthService.getValidUsername(request.getHeader(HEADER).replace(PREFIX, ""));
                    setUpSpringAuthentication(username);
                } else {
                    SecurityContextHolder.clearContext();
                }
            }
            chain.doFilter(request, response);
        } catch (Exception e) {
            GenericApiResponseDto genericApiResponseDto = new GenericApiResponseDto(e.getMessage(), 403);
            ObjectMapper objectMapper = new ObjectMapper();
            String responseJSON = objectMapper.writeValueAsString(genericApiResponseDto);
            SecurityContextHolder.clearContext();
            response.resetBuffer();
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setContentLength(responseJSON.length());
            response.getOutputStream().write(responseJSON.getBytes());
        }
    }


    private void setUpSpringAuthentication(String username) {

        List<String> authorities = Collections.singletonList("ROLE_USER");

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(username, null,
            authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
        SecurityContextHolder.getContext().setAuthentication(auth);

    }

    private boolean hasTokenHeader(HttpServletRequest httpServletRequest) {
        String authenticationHeader = httpServletRequest.getHeader(HEADER);
        return authenticationHeader != null && authenticationHeader.startsWith(PREFIX);
    }

}
