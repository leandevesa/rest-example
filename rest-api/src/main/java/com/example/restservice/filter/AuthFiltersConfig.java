package com.example.restservice.filter;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@EnableWebSecurity
@Configuration
@Order(1)
class AuthFiltersConfig extends WebSecurityConfigurerAdapter {

    private BasicAuthFilter basicAuthFilter;
    private JWTAuthorizationFilter jwtAuthorizationFilter;

    public AuthFiltersConfig(BasicAuthFilter basicAuthFilter, JWTAuthorizationFilter jwtAuthorizationFilter) {
        this.basicAuthFilter = basicAuthFilter;
        this.jwtAuthorizationFilter = jwtAuthorizationFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .addFilterAfter(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
            .addFilterAfter(basicAuthFilter, BasicAuthenticationFilter.class)
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/user").permitAll()
            .antMatchers(HttpMethod.POST, "/user/*/verify/*").permitAll()
            .antMatchers(HttpMethod.PUT, "/user").permitAll()
            .antMatchers(HttpMethod.GET, "/login**").permitAll()
            .anyRequest().authenticated()
            .and()
            .oauth2Login()
            .defaultSuccessUrl("/loginSuccess")
            .failureUrl("/loginFailure");
    }
}