package com.example.restservice.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "resource")
public class Resource implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    private String name;
    private String description;
    private String contentType;
    private String fileName;
    private boolean isPublic;

    public Resource() {
        // For Hibernate
    }

    private Resource(Builder builder) {
        id = builder.id;
        user = builder.user;
        name = builder.name;
        description = builder.description;
        contentType = builder.contentType;
        fileName = builder.fileName;
        isPublic = builder.isPublic;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(Resource copy) {
        Builder builder = new Builder();
        builder.id = copy.getId();
        builder.user = copy.getUser();
        builder.name = copy.getName();
        builder.description = copy.getDescription();
        builder.contentType = copy.getContentType();
        builder.fileName = copy.getFileName();
        builder.isPublic = copy.getIsPublic();
        return builder;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getContentType() {
        return contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean getIsPublic() {
        return isPublic;
    }

    public static final class Builder {

        private Long id;
        private User user;
        private String name;
        private String description;
        private String contentType;
        private String fileName;
        private boolean isPublic;

        private Builder() {}

        public Builder withUser(User user) {
            this.user = user;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Builder withFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public Builder withIsPublic(boolean isPublic) {
            this.isPublic = isPublic;
            return this;
        }

        public Resource build() {
            return new Resource(this);
        }
    }
}
