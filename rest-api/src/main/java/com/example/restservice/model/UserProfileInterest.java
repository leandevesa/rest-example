package com.example.restservice.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_profile_interest")
public class UserProfileInterest implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String interest;

    public UserProfileInterest() {
        // For Hibernate
    }

    public UserProfileInterest(String interest) {
        this.interest = interest;
    }

    public UserProfileInterest(Long id, String interest) {
        this.id = id;
        this.interest = interest;
    }

    public Long getId() {
        return id;
    }

    public String getInterest() {
        return interest;
    }
}
