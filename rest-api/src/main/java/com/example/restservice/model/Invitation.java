package com.example.restservice.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invitation",
       indexes = {@Index(name = "idx_to_user_invitation", columnList = "to_user_id,token"),
                 @Index(name = "idx_accepted_invitation", columnList = "accepted")})
public class Invitation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_user_id")
    private User fromUser;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "to_user_id")
    private User toUser;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "resource_id")
    private Resource resource;
    private Date expiryDate;
    @Enumerated(EnumType.STRING)
    private PermissionMode permissionMode;
    private String token;
    private boolean accepted;
    @Enumerated(EnumType.STRING)
    private InvitationType invitationType;

    public Invitation() {
        // For Hibernate
    }

    private Invitation(Builder builder) {
        id = builder.id;
        fromUser = builder.fromUser;
        toUser = builder.toUser;
        resource = builder.resource;
        expiryDate = builder.expiryDate;
        permissionMode = builder.permissionMode;
        token = builder.token;
        accepted = builder.accepted;
        invitationType = builder.invitationType;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(Invitation copy) {
        Builder builder = new Builder();
        builder.id = copy.getId();
        builder.fromUser = copy.getFromUser();
        builder.toUser = copy.getToUser();
        builder.resource = copy.getResource();
        builder.expiryDate = copy.getExpiryDate();
        builder.permissionMode = copy.getPermissionMode();
        builder.token = copy.getToken();
        builder.accepted = copy.getAccepted();
        builder.invitationType = copy.getInvitationType();
        return builder;
    }

    public Long getId() {
        return id;
    }

    public User getFromUser() {
        return fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public Resource getResource() {
        return resource;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public PermissionMode getPermissionMode() {
        return permissionMode;
    }

    public String getToken() {
        return token;
    }

    public boolean getAccepted() {
        return accepted;
    }

    public InvitationType getInvitationType() {
        return invitationType;
    }

    public static final class Builder {

        private Long id;
        private User fromUser;
        private User toUser;
        private Resource resource;
        private Date expiryDate;
        private PermissionMode permissionMode;
        private String token;
        private boolean accepted;
        private InvitationType invitationType;

        private Builder() {}

        public Builder withFromUser(User fromUser) {
            this.fromUser = fromUser;
            return this;
        }

        public Builder withToUser(User toUser) {
            this.toUser = toUser;
            return this;
        }

        public Builder withResource(Resource resource) {
            this.resource = resource;
            return this;
        }

        public Builder withExpiryDate(Date expiryDate) {
            this.expiryDate = expiryDate;
            return this;
        }

        public Builder withPermissionMode(PermissionMode permissionMode) {
            this.permissionMode = permissionMode;
            return this;
        }

        public Builder withToken(String token) {
            this.token = token;
            return this;
        }

        public Builder withAccepted(boolean accepted) {
            this.accepted = accepted;
            return this;
        }

        public Builder withInvitationType(InvitationType invitationType) {
            this.invitationType = invitationType;
            return this;
        }

        public Invitation build() {
            return new Invitation(this);
        }
    }
}
