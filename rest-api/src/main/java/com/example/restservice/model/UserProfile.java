package com.example.restservice.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_profile")
public class UserProfile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String lastname;
    private String avatar;
    private Date birthdate;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_profile_id")
    private List<UserProfileInterest> interests;

    public UserProfile() {
        // For Hibernate
    }

    private UserProfile(Builder builder) {
        id = builder.id;
        name = builder.name;
        lastname = builder.lastname;
        avatar = builder.avatar;
        birthdate = builder.birthdate;
        interests = builder.interests;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(UserProfile copy) {
        Builder builder = new Builder();
        builder.id = copy.getId();
        builder.name = copy.getName();
        builder.lastname = copy.getLastname();
        builder.avatar = copy.getAvatar();
        builder.birthdate = copy.getBirthdate();
        builder.interests = copy.getInterests();
        return builder;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAvatar() {
        return avatar;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public List<UserProfileInterest> getInterests() {
        return interests;
    }

    public static final class Builder {

        private Long id;
        private String name;
        private String lastname;
        private String avatar;
        private Date birthdate;
        private List<UserProfileInterest> interests;

        private Builder() {}

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withLastname(String lastname) {
            this.lastname = lastname;
            return this;
        }

        public Builder withAvatar(String avatar) {
            this.avatar = avatar;
            return this;
        }

        public Builder withBirthdate(Date birthdate) {
            this.birthdate = birthdate;
            return this;
        }

        public Builder withInterests(List<UserProfileInterest> interests) {
            this.interests = interests;
            return this;
        }

        public UserProfile build() {
            return new UserProfile(this);
        }
    }
}
