package com.example.restservice.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "verification_token",
       uniqueConstraints={
           @UniqueConstraint(name = "idx_verification_token", columnNames ={"token"})
       })
public class VerificationToken implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String token;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    private Date expiryDate;

    public VerificationToken() {
        // For Hibernate
    }

    private VerificationToken(Builder builder) {
        token = builder.token;
        user = builder.user;
        expiryDate = builder.expiryDate;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(VerificationToken copy) {
        Builder builder = new Builder();
        builder.token = copy.getToken();
        builder.user = copy.getUser();
        builder.expiryDate = copy.getExpiryDate();
        return builder;
    }

    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public static final class Builder {

        private String token;
        private User user;
        private Date expiryDate;

        private Builder() {}

        public Builder withToken(String token) {
            this.token = token;
            return this;
        }

        public Builder withUser(User user) {
            this.user = user;
            return this;
        }

        public Builder withExpiryDate(Date expiryDate) {
            this.expiryDate = expiryDate;
            return this;
        }

        public VerificationToken build() {
            return new VerificationToken(this);
        }
    }
}