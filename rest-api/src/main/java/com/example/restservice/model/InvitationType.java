package com.example.restservice.model;

public enum InvitationType {
    SEND,
    ASK
}
