package com.example.restservice.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "permission",
       uniqueConstraints={
           @UniqueConstraint(name = "idx_permission_user_resource", columnNames ={"user_id","resource_id"})
       })
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    @Enumerated(EnumType.STRING)
    private PermissionMode permissionMode;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "resource_id")
    private Resource resource;

    public Permission() {
        // For Hibernate
    }

    public User getUser() {
        return user;
    }

    public PermissionMode getPermissionMode() {
        return permissionMode;
    }

    public Resource getResource() {
        return resource;
    }

    private Permission(Builder builder) {
        user = builder.user;
        permissionMode = builder.permissionMode;
        resource = builder.resource;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(Permission copy) {
        Builder builder = new Builder();
        builder.user = copy.getUser();
        builder.permissionMode = copy.getPermissionMode();
        builder.resource = copy.getResource();
        return builder;
    }

    public static final class Builder {

        private User user;
        private PermissionMode permissionMode;
        private Resource resource;

        private Builder() {}

        public Builder withUser(User user) {
            this.user = user;
            return this;
        }

        public Builder withPermissionMode(PermissionMode permissionMode) {
            this.permissionMode = permissionMode;
            return this;
        }

        public Builder withResource(Resource resource) {
            this.resource = resource;
            return this;
        }

        public Permission build() {
            return new Permission(this);
        }
    }
}
