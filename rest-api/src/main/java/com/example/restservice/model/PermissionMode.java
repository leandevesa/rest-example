package com.example.restservice.model;

public enum PermissionMode {
    READ,
    READ_WRITE
}
