package com.example.restservice.translator;

import com.example.restservice.dto.user.UserDto;
import com.example.restservice.dto.user.UserProfileDto;
import com.example.restservice.dto.user.UserRegisterRequestDto;
import com.example.restservice.model.User;
import com.example.restservice.model.UserProfile;
import com.example.restservice.model.UserProfileInterest;
import com.example.restservice.service.PasswordHashingService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserTranslator {

    private PasswordHashingService passwordHashingService;

    public UserTranslator(PasswordHashingService passwordHashingService) {
        this.passwordHashingService = passwordHashingService;
    }

    public User fromUserRegisterDto(UserRegisterRequestDto userRegisterRequestDto) {

        String hashedPassword = passwordHashingService.hash(userRegisterRequestDto.getPassword());

        User user = fromUserDto(userRegisterRequestDto, true);

        return User.newBuilder(user)
                   .withPassword(hashedPassword)
                   .build();
    }

    public User fromUserDto(UserDto userDto, boolean validateMandatoryFields) {

        // TODO: Validate mandatory fields

        UserProfileDto userProfileDto = (userDto.getUserProfile() != null ? userDto.getUserProfile() : UserProfileDto.newBuilder().build());

        List<UserProfileInterest> userProfileInterests = userProfileDto.getInterests().stream()
                                                                       .map(UserProfileInterest::new)
                                                                       .collect(Collectors.toList());

        UserProfile userProfile = UserProfile.newBuilder()
                                             .withAvatar(userProfileDto.getAvatar())
                                             .withName(userProfileDto.getName())
                                             .withLastname(userProfileDto.getLastname())
                                             .withBirthdate(userProfileDto.getBirthdate())
                                             .withInterests(userProfileInterests)
                                             .build();

        return User.newBuilder()
                   .withUsername(userDto.getUsername())
                   .withEmail(userDto.getEmail())
                   .withUserProfile(userProfile)
                   .withVerified(false)
                   .withDeleted(false)
                   .build();
    }

    public UserDto toUserDto(User user) {

        UserProfile userProfile = user.getUserProfile();

        List<String> userInterests = userProfile.getInterests().stream()
                                                .map(UserProfileInterest::getInterest)
                                                .collect(Collectors.toList());

        UserProfileDto userProfileDto = UserProfileDto.newBuilder()
                                                      .withAvatar(userProfile.getAvatar())
                                                      .withName(userProfile.getName())
                                                      .withLastname(userProfile.getLastname())
                                                      .withBirthdate(userProfile.getBirthdate())
                                                      .withInterests(userInterests)
                                                      .build();

        return UserDto.newBuilder()
                      .withUsername(user.getUsername())
                      .withEmail(user.getEmail())
                      .withUserProfile(userProfileDto)
                      .build();
    }
}
