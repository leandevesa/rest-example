package com.example.restservice.translator;

import com.example.restservice.dto.resource.ResourceDto;
import com.example.restservice.model.Resource;
import org.springframework.stereotype.Service;

@Service
public class ResourceTranslator {
    public ResourceDto toResourceDto(Resource resource) {
        return ResourceDto.newBuilder()
                          .withContentType(resource.getContentType())
                          .withDescription(resource.getDescription())
                          .withFileName(resource.getFileName())
                          .withId(resource.getId())
                          .withIsPublic(resource.getIsPublic())
                          .withName(resource.getName())
                          .withUsername(resource.getUser().getUsername())
                          .build();
    }
}
