package com.example.restservice.translator;

import com.example.restservice.dto.invitation.PendingInvitationDto;
import com.example.restservice.dto.invitation.InviteUserDto;
import com.example.restservice.model.Invitation;
import com.example.restservice.model.InvitationType;
import com.example.restservice.model.PermissionMode;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public class InvitationTranslator {

    private final static int SEVEN_DAYS = 7;

    public Invitation fromInvitationsDto(PermissionMode permissionMode, User fromUser, User toUser, Resource resource,
                                         InvitationType invitationType) {

        String token = UUID.randomUUID().toString();
        Date expiryDate = getExpiryDate();

        return Invitation.newBuilder()
                         .withPermissionMode(permissionMode)
                         .withFromUser(fromUser)
                         .withToUser(toUser)
                         .withResource(resource)
                         .withExpiryDate(expiryDate)
                         .withToken(token)
                         .withAccepted(false)
                         .withInvitationType(invitationType)
                         .build();
    }

    public PendingInvitationDto toPendingInvitationDto(Invitation invitation) {
        return new PendingInvitationDto(invitation.getFromUser().getUsername(),
                                        invitation.getResource().getName(),
                                        invitation.getToken(),
                                        invitation.getInvitationType());
    }

    private Date getExpiryDate() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, SEVEN_DAYS);
        return cal.getTime();
    }
}
