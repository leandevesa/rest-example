package com.example.restservice.repository;

import com.example.restservice.model.Permission;
import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionRepository extends JpaRepository<Permission, Long> {
    Optional<Permission> findByUserAndResource(User user, Resource resource);
}
