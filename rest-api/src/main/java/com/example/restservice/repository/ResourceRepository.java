package com.example.restservice.repository;

import com.example.restservice.model.Resource;
import com.example.restservice.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResourceRepository extends JpaRepository<Resource, Long> {
    List<Resource> findByIsPublic(boolean isPublic);
}
