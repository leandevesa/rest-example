package com.example.restservice.repository;

import com.example.restservice.model.User;
import com.example.restservice.model.VerificationToken;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    Optional<VerificationToken> findByTokenAndUser(String token, User user);
}
