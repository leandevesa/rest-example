package com.example.restservice.repository;

import com.example.restservice.model.Invitation;
import com.example.restservice.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvitationRepository extends JpaRepository<Invitation, Long> {
    Optional<Invitation> findByToUserAndToken(User toUser, String token);
    List<Invitation> findByAcceptedAndToUser(boolean accepted, User toUser);
}
